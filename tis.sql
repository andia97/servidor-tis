-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-07-2020 a las 22:54:08
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agregarrequisito`
--

DROP TABLE IF EXISTS `agregarrequisito`;
CREATE TABLE `agregarrequisito` (
  `id` int(11) NOT NULL,
  `id_req` int(11) DEFAULT NULL,
  `id_conv` int(11) NOT NULL,
  `req_extra` varchar(45) DEFAULT NULL,
  `estado` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `agregarrequisito`
--

INSERT INTO `agregarrequisito` (`id`, `id_req`, `id_conv`, `req_extra`, `estado`) VALUES
(89, 3, 21, NULL, 'false'),
(90, 1, 21, NULL, 'false'),
(92, NULL, 21, 'Fotocopia de certificado de nacimiento', 'false'),
(93, 1, 26, NULL, 'false'),
(94, 2, 26, NULL, 'false'),
(95, NULL, 21, 'Credencial de conducir', 'false'),
(97, NULL, 26, 'Factura de Luz', 'false'),
(98, NULL, 26, 'Aportes AFP', 'false'),
(99, NULL, 21, 'Certificado de Seguro de Salud Universitario', 'false'),
(101, NULL, 21, NULL, 'false'),
(110, 1, 40, NULL, 'false'),
(111, 1, 41, NULL, 'false'),
(112, 2, 41, NULL, 'false'),
(113, 5, 41, NULL, 'false'),
(114, NULL, 41, 'Requisito Extra Prueba Ariel', 'false'),
(115, 1, 44, NULL, 'false'),
(117, 3, 44, NULL, 'false'),
(119, 2, 44, NULL, 'false'),
(123, 2, 45, NULL, 'false'),
(124, 5, 46, NULL, 'false'),
(125, 4, 46, NULL, 'false'),
(126, NULL, 46, 'Presentar certificado de condición de estudia', 'false'),
(127, 1, 47, NULL, 'false'),
(128, 2, 47, NULL, 'false'),
(129, 4, 21, NULL, 'false'),
(130, NULL, 21, 'asdasdasd', 'false'),
(131, 5, 26, NULL, 'false'),
(132, NULL, 26, 'requisito no alfanumerico', 'false'),
(133, 1, 42, NULL, 'false'),
(134, 2, 42, NULL, 'false'),
(135, NULL, 21, 'qweqwe', 'false'),
(136, 1, 49, NULL, 'false'),
(137, 2, 49, NULL, 'false'),
(138, 1, 50, NULL, 'false'),
(139, 2, 50, NULL, 'false'),
(140, 1, 45, NULL, 'false');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigo`
--

DROP TABLE IF EXISTS `codigo`;
CREATE TABLE `codigo` (
  `id` int(11) NOT NULL,
  `codigo` varchar(5) NOT NULL,
  `tipo` varchar(10) NOT NULL,
  `estado` varchar(6) NOT NULL DEFAULT 'Activo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `codigo`
--

INSERT INTO `codigo` (`id`, `codigo`, `tipo`, `estado`) VALUES
(1, 'tPU50', 'Comision', 'Usado'),
(33, 'aANUS', 'Postulante', 'Activo'),
(34, 'UFQP2', 'Postulante', 'Usado'),
(35, 'Kac0K', 'Comision', 'Activo'),
(36, 'a1JZ4', 'Postulante', 'Activo'),
(37, 'bRZ4c', 'Comision', 'Activo'),
(38, 'DXTRp', 'Postulante', 'Activo'),
(39, 'cWWsn', 'Postulante', 'Activo'),
(40, 'tSoBX', 'Comision', 'Activo'),
(41, 'OTJoU', 'Postulante', 'Activo'),
(42, 'AaFsy', 'Comision', 'Activo'),
(43, 'NHyXc', 'Postulante', 'Activo'),
(44, 'HmVJm', 'Postulante', 'Activo'),
(45, '5G9dF', 'Postulante', 'Activo'),
(46, 'RCIn4', 'Comision', 'Activo'),
(47, 'B94c1', 'Comision', 'Activo'),
(48, 'tBuDD', 'Postulante', 'Activo'),
(49, 'BkLyY', 'Postulante', 'Activo'),
(50, '37tfw', 'Postulante', 'Activo'),
(51, 'dzMlO', 'Comision', 'Activo'),
(52, 'HqW6P', 'Postulante', 'Activo'),
(53, 'NCcDZ', 'Comision', 'Activo'),
(54, 'Aynl7', 'Postulante', 'Activo'),
(55, 'Fykln', 'Comision', 'Activo'),
(56, 'qnXMA', 'Postulante', 'Activo'),
(57, 'gpdOU', 'Comision', 'Activo'),
(58, 'WD1To', 'Comision', 'Activo'),
(59, '8sv0O', 'Comision', 'Activo'),
(60, 'jK2zq', 'Comision', 'Activo'),
(61, 'OKthO', 'Postulante', 'Activo'),
(62, 'nQFSu', 'Postulante', 'Activo'),
(63, '0WQNr', 'Comision', 'Activo'),
(64, '5W09H', 'Postulante', 'Activo'),
(65, '3GDnC', 'Comision', 'Activo'),
(66, '0b3DC', 'Postulante', 'Activo'),
(67, 'NB57V', 'Comision', 'Activo'),
(68, 'X2zXC', 'Postulante', 'Activo'),
(69, 'kaX2P', 'Postulante', 'Activo'),
(70, 'pE4hg', 'Postulante', 'Activo'),
(71, 'J2xyJ', 'Postulante', 'Activo'),
(72, 'DJMpr', 'Postulante', 'Activo'),
(73, 'DxAzv', 'Postulante', 'Activo'),
(74, 'QPiwJ', 'Postulante', 'Activo'),
(75, 'n6E9T', 'Postulante', 'Activo'),
(76, 'wTG91', 'Postulante', 'Activo'),
(77, 'L3cTY', 'Postulante', 'Activo'),
(78, 'CvZuc', 'Comision', 'Activo'),
(79, 'DkTYZ', 'Postulante', 'Activo'),
(80, 'isHZX', 'Comision', 'Activo'),
(81, 'VET0j', 'Comision', 'Activo'),
(82, 'pKaob', 'Comision', 'Activo'),
(83, 'Xcf0l', 'Comision', 'Activo'),
(84, 'gIHLd', 'Postulante', 'Activo'),
(85, 'CPpZu', 'Postulante', 'Activo'),
(86, 'wG8wG', 'Comision', 'Activo'),
(87, 'TL8bz', 'Postulante', 'Activo'),
(88, 'Rusfl', 'Comision', 'Activo'),
(89, 'CTPxg', 'Postulante', 'Activo'),
(90, 'wpMHj', 'Comision', 'Activo'),
(91, 'rb1Lu', 'Postulante', 'Activo'),
(92, 'rZD0F', 'Comision', 'Activo'),
(93, '6vcXG', 'Postulante', 'Activo'),
(94, 'HNA76', 'Postulante', 'Activo'),
(95, 'Cjozz', 'Postulante', 'Activo'),
(96, 'Sgamg', 'Postulante', 'Activo'),
(97, 'EXWWn', 'Postulante', 'Activo'),
(98, 'Pl8b7', 'Postulante', 'Activo'),
(99, 'jeNmP', 'Postulante', 'Activo'),
(100, 'sVYkl', 'Postulante', 'Activo'),
(101, 'msdYA', 'Postulante', 'Activo'),
(102, 'iGHVA', 'Postulante', 'Activo'),
(103, 'nirr0', 'Postulante', 'Activo'),
(104, 'NuACi', 'Postulante', 'Activo'),
(105, 'yCl5m', 'Postulante', 'Activo'),
(106, 'iqVJh', 'Postulante', 'Activo'),
(107, 'zVwuu', 'Postulante', 'Activo'),
(108, 'DmpUo', 'Postulante', 'Activo'),
(109, 'pQ1Uk', 'Postulante', 'Activo'),
(110, 'UxZJE', 'Comision', 'Activo'),
(111, 'tLBjg', 'Comision', 'Activo'),
(112, 'G6ZBn', 'Postulante', 'Activo'),
(113, 'MA79C', 'Postulante', 'Activo'),
(114, 'MBG4G', 'Comision', 'Activo'),
(115, '5ZnVW', 'Postulante', 'Activo'),
(116, 'C1zI6', 'Comision', 'Activo'),
(117, '5uS2g', 'Postulante', 'Activo'),
(118, 'ROsqF', 'Postulante', 'Activo'),
(119, 'CWw12', 'Postulante', 'Activo'),
(120, 'eV6pb', 'Postulante', 'Activo'),
(121, 'sFi8h', 'Postulante', 'Activo'),
(122, 'oBsgI', 'Postulante', 'Activo'),
(123, 'ciy0r', 'Postulante', 'Activo'),
(124, 'b4fD7', 'Comision', 'Activo'),
(125, 'ZcFii', 'Comision', 'Activo'),
(126, 'k8yhZ', 'Postulante', 'Activo'),
(127, 'H5BqH', 'Comision', 'Activo'),
(128, '3wcOO', 'Comision', 'Activo'),
(129, 'ruQvQ', 'Postulante', 'Activo'),
(130, 'uiD8V', 'Comision', 'Activo'),
(131, 'gTVxT', 'Postulante', 'Activo'),
(132, 'D7vvM', 'Comision', 'Activo'),
(133, 'pS4tC', 'Postulante', 'Activo'),
(134, 'LdMNm', 'Comision', 'Activo'),
(135, 's4o74', 'Comision', 'Activo'),
(136, 'VfHSD', 'Postulante', 'Activo'),
(137, 'xPVta', 'Comision', 'Activo'),
(138, 'LOboF', 'Postulante', 'Activo'),
(139, 'WI0Jc', 'Comision', 'Activo'),
(140, '6UyZc', 'Comision', 'Activo'),
(141, 'wr9tX', 'Postulante', 'Activo'),
(142, '0JRwl', 'Comision', 'Activo'),
(143, '6RLaT', 'Postulante', 'Activo'),
(144, '1Aeya', 'Comision', 'Activo'),
(145, 'oyuvW', 'Comision', 'Activo'),
(146, 't20dq', 'Postulante', 'Activo'),
(147, 'B0eDq', 'Postulante', 'Activo'),
(148, 'Nt5X3', 'Postulante', 'Activo'),
(149, 'tpPob', 'Postulante', 'Activo'),
(150, 'S0TRm', 'Postulante', 'Activo'),
(151, 'QuGpy', 'Postulante', 'Activo'),
(152, '7pchP', 'Postulante', 'Activo'),
(153, 'RRe43', 'Postulante', 'Activo'),
(154, 'z72gy', 'Postulante', 'Activo'),
(155, 'q4qop', 'Comision', 'Activo'),
(156, 'uZPrf', 'Comision', 'Activo'),
(157, 'yxRrp', 'Postulante', 'Activo'),
(158, 'QGwwm', 'Postulante', 'Activo'),
(159, 'uIUjS', 'Comision', 'Activo'),
(160, '3CWmO', 'Comision', 'Activo'),
(161, 'B87ov', 'Comision', 'Activo'),
(162, 'Qbexb', 'Comision', 'Activo'),
(163, '9Nx5b', 'Comision', 'Activo'),
(164, 'GJ5zu', 'Postulante', 'Activo'),
(165, 'G0zQs', 'Postulante', 'Activo'),
(166, 'L7DO1', 'Comision', 'Activo'),
(167, '9sNAi', 'Postulante', 'Activo'),
(168, 'DuDnE', 'Postulante', 'Activo'),
(169, 'KRi9q', 'Postulante', 'Activo'),
(170, '1Cu1i', 'Postulante', 'Activo'),
(171, 'TT7IW', 'Postulante', 'Activo'),
(172, 'LQSza', 'Postulante', 'Activo'),
(173, 'Jl4G6', 'Postulante', 'Activo'),
(174, 'Of76a', 'Postulante', 'Activo'),
(175, 'pdgOU', 'Postulante', 'Activo'),
(176, '3Pyl8', 'Postulante', 'Activo'),
(177, 'XwdlZ', 'Postulante', 'Activo'),
(178, '7LGh6', 'Postulante', 'Activo'),
(179, 'ZbNWu', 'Postulante', 'Activo'),
(180, 'TMngv', 'Postulante', 'Activo'),
(181, '6i3my', 'Postulante', 'Activo'),
(182, 'ATSeA', 'Postulante', 'Activo'),
(183, 'kNrb1', 'Postulante', 'Activo'),
(184, 'YXpke', 'Postulante', 'Activo'),
(185, 'ed51P', 'Postulante', 'Activo'),
(186, '7XMK1', 'Postulante', 'Activo'),
(187, '0rYwM', 'Postulante', 'Activo'),
(188, 'hNtlc', 'Postulante', 'Activo'),
(189, 'nVwhO', 'Postulante', 'Activo'),
(190, 'XsHLT', 'Postulante', 'Activo'),
(191, 'aswja', 'Postulante', 'Activo'),
(192, 'alDbZ', 'Postulante', 'Activo'),
(193, 'U28sz', 'Postulante', 'Activo'),
(194, 'Lq4Q5', 'Postulante', 'Activo'),
(195, 'hbehg', 'Postulante', 'Activo'),
(196, 'whpqy', 'Postulante', 'Activo'),
(197, 'hS0CQ', 'Postulante', 'Activo'),
(198, 'VYVTD', 'Postulante', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comision`
--

DROP TABLE IF EXISTS `comision`;
CREATE TABLE `comision` (
  `id` int(8) NOT NULL,
  `id_user` int(8) NOT NULL,
  `id_conv` int(8) NOT NULL,
  `id_rol` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comision`
--

INSERT INTO `comision` (`id`, `id_user`, `id_conv`, `id_rol`) VALUES
(1, 2, 1, 1),
(2, 2, 2, 2),
(3, 59, 21, 2),
(6, 60, 21, 2),
(7, 61, 21, 3),
(8, 62, 26, 1),
(9, 59, 26, 1),
(10, 59, 40, 2),
(11, 59, 49, 1),
(12, 59, 41, 2),
(13, 59, 21, 1),
(14, 59, 26, 2),
(15, 71, 44, 1),
(16, 71, 42, 2),
(17, 71, 44, 2),
(18, 59, 45, 1),
(19, 59, 45, 2),
(20, 61, 45, 1),
(21, 61, 45, 2),
(22, 75, 45, 1),
(23, 75, 45, 2),
(24, 75, 44, 1),
(25, 77, 46, 1),
(26, 77, 46, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `convocatoria`
--

DROP TABLE IF EXISTS `convocatoria`;
CREATE TABLE `convocatoria` (
  `id` int(11) NOT NULL,
  `nombre_conv` varchar(500) NOT NULL,
  `facultad` varchar(500) NOT NULL,
  `carrera` varchar(500) NOT NULL,
  `departamento` varchar(500) NOT NULL,
  `fecha_actual` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_limite` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `convocatoria`
--

INSERT INTO `convocatoria` (`id`, `nombre_conv`, `facultad`, `carrera`, `departamento`, `fecha_actual`, `fecha_limite`) VALUES
(45, 'Convocatoria Fake Jose', 'Ciencias y Tecnologia', 'Sistemas', 'Informatica y Sistemas', '2020-08-23 00:13:55', '2020-09-23 00:13:55'),
(49, 'Prueba Upload', 'Ciencias y Tecnologia', 'Informatica', 'Informatica y Sistemas', '2020-07-26 16:34:48', '2020-07-27 16:34:48'),
(50, 'PruebaUpload Ariel ', 'Ciencias y Tecnologia', 'Informatica', 'Informatica y Sistemas', '2020-07-26 04:56:11', '2020-07-26 04:56:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fileuploads`
--

DROP TABLE IF EXISTS `fileuploads`;
CREATE TABLE `fileuploads` (
  `id` int(11) NOT NULL,
  `id_insc_conv` int(11) NOT NULL,
  `name_req` varchar(200) NOT NULL,
  `file_dir` varchar(250) NOT NULL,
  `tipo` varchar(12) NOT NULL,
  `estado` varchar(8) DEFAULT NULL,
  `observacion` varchar(140) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `fileuploads`
--

INSERT INTO `fileuploads` (`id`, `id_insc_conv`, `name_req`, `file_dir`, `tipo`, `estado`, `observacion`) VALUES
(21, 157, 'Fotocopia de carnet de identidad ', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FFotocopia%20de%20carnet%20de%20identidad%20-User63-Conv46?alt=media&token=a8b77c28-53a5-4a16-9ca2-3dfb10eb418d', 'obligatorio', 'true', ''),
(22, 157, 'Certificado de no adeudado a la Biblioteca', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FCertificado%20de%20no%20adeudado%20a%20la%20Biblioteca-User63-Conv46?alt=media&token=1025f19f-8b99-440b-9a76-558a7e98aa65', 'obligatorio', 'true', ''),
(23, 158, 'Solicitud de auxiliatura', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FSolicitud%20de%20auxiliatura-User63-Conv45?alt=media&token=86c0174c-4786-46da-9342-356c209a085c', 'obligatorio', 'true', ''),
(24, 158, 'Kardex', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FKardex-User63-Conv45?alt=media&token=9d0fc6e9-83e4-4e0e-af05-6039d3a80188', 'obligatorio', 'true', ''),
(25, 159, 'Fotocopia de carnet de identidad ', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FFotocopia%20de%20carnet%20de%20identidad%20-User64-Conv46?alt=media&token=7c668ac3-f8e4-4b35-a116-13197e0547db', 'obligatorio', 'true', ''),
(26, 159, 'Certificado de no adeudado a la Biblioteca', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FCertificado%20de%20no%20adeudado%20a%20la%20Biblioteca-User64-Conv46?alt=media&token=9cee01da-4388-431a-845c-286b68308f13', 'obligatorio', 'true', ''),
(27, 160, 'Solicitud de auxiliatura', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FSolicitud%20de%20auxiliatura-User64-Conv45?alt=media&token=5ad19fa2-91ec-4d29-8259-f513caca1dcd', 'obligatorio', 'true', ''),
(28, 160, 'Kardex', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FKardex-User64-Conv45?alt=media&token=5c08d71d-74ed-4c33-85cc-e0784dcc8653', 'obligatorio', 'true', ''),
(29, 162, 'Kardex', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FKardex-User67-Conv45?alt=media&token=9ec41540-20b2-42ad-9f00-d9ceee619e98', 'obligatorio', 'false', ''),
(30, 164, 'Kardex', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FKardex-User67-Conv45?alt=media&token=472c610d-af77-4166-9db6-dbde6e2c6706', 'obligatorio', 'true', ''),
(31, 164, 'Solicitud de auxiliatura', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FSolicitud%20de%20auxiliatura-User67-Conv45?alt=media&token=92c76637-6ca0-4b84-8ad2-40aaaab71a7b', 'obligatorio', 'true', ''),
(32, 165, 'Fotocopia de carnet de identidad ', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FFotocopia%20de%20carnet%20de%20identidad%20-User65-Conv46?alt=media&token=72edefd5-6dcb-49a4-8d59-7e760dae2af3', 'obligatorio', 'true', ''),
(33, 166, 'Fotocopia de carnet de identidad ', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FFotocopia%20de%20carnet%20de%20identidad%20-User65-Conv46?alt=media&token=43f89991-9d26-462e-85b6-5d2faabb90b2', 'obligatorio', 'true', ''),
(34, 166, 'Certificado de no adeudado a la Biblioteca', 'https://firebasestorage.googleapis.com/v0/b/tis-filecloud.appspot.com/o/files%2FCertificado%20de%20no%20adeudado%20a%20la%20Biblioteca-User65-Conv46?alt=media&token=cb0bdc39-a950-4989-b1b1-e0844d625a46', 'obligatorio', 'true', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcionconvocatoria`
--

DROP TABLE IF EXISTS `inscripcionconvocatoria`;
CREATE TABLE `inscripcionconvocatoria` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_conv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `inscripcionconvocatoria`
--

INSERT INTO `inscripcionconvocatoria` (`id`, `id_user`, `id_conv`) VALUES
(157, 63, 46),
(158, 63, 45),
(159, 64, 46),
(160, 64, 45),
(164, 67, 45);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion_item`
--

DROP TABLE IF EXISTS `inscripcion_item`;
CREATE TABLE `inscripcion_item` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_conv` int(11) NOT NULL,
  `id_item` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `inscripcion_item`
--

INSERT INTO `inscripcion_item` (`id`, `id_user`, `id_conv`, `id_item`) VALUES
(1, 50, 21, 11),
(2, 63, 21, 11),
(3, 2, 21, 2),
(4, 66, 21, 11),
(5, 50, 21, 18),
(6, 50, 21, 13),
(7, 50, 21, 17),
(8, 63, 41, 21),
(9, 64, 41, 21),
(10, 65, 41, 21),
(11, 67, 41, 21),
(12, 50, 26, 14),
(13, 65, 21, 11),
(14, 70, 44, 22),
(15, 50, 44, 23),
(16, 67, 44, 23),
(17, 64, 45, 24),
(18, 63, 45, 24),
(19, 67, 45, 24),
(20, 66, 45, 24),
(21, 65, 45, 24),
(22, 65, 45, 25),
(23, 64, 45, 25),
(24, 76, 46, 28),
(25, 80, 47, 30),
(26, 63, 21, 26),
(27, 63, 21, 13),
(28, 63, 45, 25),
(29, 63, 46, 28),
(30, 63, 21, 15),
(31, 70, 46, 28);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `id_conv` int(11) NOT NULL,
  `nombre_item` varchar(500) NOT NULL,
  `hora_laboral` int(11) NOT NULL,
  `nro_items` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `item`
--

INSERT INTO `item` (`id`, `id_conv`, `nombre_item`, `hora_laboral`, `nro_items`) VALUES
(11, 21, 'Administrador de Laboratorio de Cómputo', 80, 7),
(13, 21, 'Administrador de Laboratorio de Desarrollo', 80, 2),
(14, 26, 'Auxiliar de Introduccion a la programacion ', 36, 7),
(15, 21, 'Auxiliar de Laboratorio de   Desarrollo ', 56, 2),
(16, 21, 'Administrador de Laboratorio de Mantenimiento de Software', 80, 1),
(17, 21, 'Auxiliar de Laboratorio de Mantenimiento de Software', 32, 4),
(18, 21, 'Administrador de Laboratorio de Mantenimiento de Hardware ', 80, 1),
(19, 21, 'Auxiliar de Laboratorio de Mantenimiento de Hardware', 32, 4),
(20, 21, 'Auxiliar de Terminal de Laboratorio de Computo', 80, 1),
(21, 41, 'Item Prueba Ariel', 80, 7),
(22, 44, 'Administrador de Laboratorio de Computo ', 80, 7),
(23, 44, 'Administrador de Laboratorio deDesarrollo', 80, 2),
(24, 45, 'Item prueba numero uno', 20, 2),
(25, 45, 'item prueba numero dos', 40, 5),
(26, 21, 'Este es un test', 45, 45),
(27, 26, 'Administrador de Laboratorio de  Computo', 80, 7),
(28, 46, 'Administrador de Laboratorio de  Desarrollo', 80, 2),
(29, 46, 'Administrador de Laboratorio de  Desarrollo', 80, 2),
(30, 47, 'Administrador de Laboratorio de Computo Examen', 80, 7),
(31, 50, 'Administrador de Laboratorio de Cómputo   ', 80, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_habilitados`
--

DROP TABLE IF EXISTS `lista_habilitados`;
CREATE TABLE `lista_habilitados` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_conv` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `lista_habilitados`
--

INSERT INTO `lista_habilitados` (`id`, `id_user`, `id_conv`, `estado`) VALUES
(6, 65, 21, 'true'),
(7, 66, 21, 'true'),
(8, 50, 21, 'true'),
(9, 63, 21, 'false'),
(10, 50, 40, 'true'),
(11, 63, 41, 'true'),
(12, 64, 41, 'false'),
(13, 67, 41, 'true'),
(14, 64, 21, 'false'),
(15, 50, 26, 'true'),
(16, 51, 44, 'false'),
(17, 70, 44, 'true'),
(18, 50, 44, 'true'),
(19, 67, 44, 'true'),
(20, 64, 45, 'true'),
(21, 66, 45, 'false'),
(22, 67, 45, 'true'),
(23, 63, 45, 'false'),
(25, 76, 46, 'true'),
(26, 80, 47, 'true'),
(27, 63, 46, 'true'),
(28, 64, 46, 'true');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `meritos_seccion`
--

DROP TABLE IF EXISTS `meritos_seccion`;
CREATE TABLE `meritos_seccion` (
  `id` int(11) NOT NULL,
  `id_seccion` int(11) NOT NULL,
  `id_conv` int(11) NOT NULL,
  `puntos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `meritos_seccion`
--

INSERT INTO `meritos_seccion` (`id`, `id_seccion`, `id_conv`, `puntos`) VALUES
(3, 15, 12, 15),
(5, 16, 12, 85),
(15, 1, 50, 50),
(16, 1, 49, 45),
(17, 2, 49, 55),
(18, 2, 50, 50);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nota_rendimiento`
--

DROP TABLE IF EXISTS `nota_rendimiento`;
CREATE TABLE `nota_rendimiento` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_rendimiento` int(11) NOT NULL,
  `id_conv` int(8) NOT NULL,
  `nota` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nota_rendimiento`
--

INSERT INTO `nota_rendimiento` (`id`, `id_user`, `id_rendimiento`, `id_conv`, `nota`) VALUES
(36, 50, 8, 21, 15),
(37, 66, 8, 21, 12),
(38, 66, 11, 21, 15),
(39, 66, 15, 21, 9),
(40, 65, 8, 21, 15),
(41, 65, 11, 21, 25),
(42, 65, 15, 21, 5),
(43, 50, 17, 26, 12),
(44, 50, 11, 21, 19),
(45, 50, 15, 21, 8),
(46, 67, 21, 44, 29),
(47, 67, 22, 44, 25),
(48, 64, 24, 45, 8),
(49, 64, 25, 45, 25),
(50, 65, 24, 45, 2),
(51, 65, 25, 45, 3),
(52, 76, 26, 46, 31),
(53, 76, 27, 46, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nota_tematica`
--

DROP TABLE IF EXISTS `nota_tematica`;
CREATE TABLE `nota_tematica` (
  `id` int(8) NOT NULL,
  `id_user` int(8) NOT NULL,
  `id_tematica` int(8) NOT NULL,
  `id_item` int(8) NOT NULL,
  `nota` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nota_tematica`
--

INSERT INTO `nota_tematica` (`id`, `id_user`, `id_tematica`, `id_item`, `nota`) VALUES
(6, 50, 7, 11, 11),
(7, 66, 7, 11, 15),
(8, 63, 13, 21, 27),
(9, 67, 13, 21, 21),
(10, 50, 14, 13, 9),
(11, 50, 8, 11, 20),
(12, 50, 9, 11, 16),
(13, 50, 10, 11, 25),
(14, 50, 11, 11, 5),
(15, 65, 7, 11, 15),
(16, 65, 8, 11, 18),
(17, 50, 15, 14, 8),
(18, 67, 22, 23, 22),
(19, 67, 23, 23, 30),
(20, 50, 22, 23, 20),
(21, 50, 23, 23, 20),
(22, 64, 25, 24, 36),
(23, 64, 26, 24, 50),
(24, 65, 25, 24, 50),
(25, 65, 26, 24, 23),
(26, 67, 25, 24, 26),
(27, 67, 26, 24, 50),
(28, 64, 27, 25, 5),
(29, 64, 28, 25, 10),
(30, 65, 27, 25, 25),
(31, 65, 28, 25, 30),
(32, 76, 29, 28, 20),
(33, 76, 30, 28, 15),
(34, 76, 31, 28, 10),
(35, 76, 32, 28, 20),
(36, 76, 33, 28, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `punto_experiencia`
--

DROP TABLE IF EXISTS `punto_experiencia`;
CREATE TABLE `punto_experiencia` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_req` int(11) NOT NULL,
  `id_conv` int(8) NOT NULL,
  `punto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `punto_experiencia`
--

INSERT INTO `punto_experiencia` (`id`, `id_user`, `id_req`, `id_conv`, `punto`) VALUES
(8, 50, 12, 21, 2),
(9, 50, 14, 26, 4),
(10, 50, 15, 26, 2),
(11, 50, 16, 26, 2),
(12, 66, 12, 21, 2),
(13, 66, 19, 21, 6),
(14, 66, 20, 21, 2),
(15, 66, 21, 21, 3),
(16, 66, 23, 21, 10),
(17, 65, 12, 21, 4),
(18, 50, 19, 21, 4),
(19, 65, 19, 21, 4),
(20, 64, 25, 45, 2),
(21, 64, 26, 45, 12),
(22, 64, 27, 45, 8),
(23, 65, 25, 45, 1),
(24, 65, 26, 45, 4),
(25, 65, 27, 45, 4),
(26, 76, 28, 46, 3),
(27, 76, 29, 46, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rendimiento_academico`
--

DROP TABLE IF EXISTS `rendimiento_academico`;
CREATE TABLE `rendimiento_academico` (
  `id` int(8) NOT NULL,
  `id_conv` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `puntos` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rendimiento_academico`
--

INSERT INTO `rendimiento_academico` (`id`, `id_conv`, `nombre`, `puntos`) VALUES
(7, '41', 'Rendimiento Academico Prueba Ariel ', '21'),
(8, '21', 'Promedio General semestre 1-2019', '25'),
(9, '43', 'Promedio General ', '35'),
(10, '43', 'Promedio Semestre 1-2020', '30'),
(11, '21', 'Promedio Semestre 2-2017', '30'),
(12, '41', 'Porcentaje de prueba ', '12'),
(13, '41', 'Porcentaje de prueba 2', '23'),
(14, '41', 'Porcentaje de Prueba 5', '9'),
(16, '42', 'Prueba', '10'),
(17, '26', 'Promedio Gestion 2020', '25'),
(21, '44', 'Promedio general de materias en el periodo académi', '30'),
(22, '44', 'prueba', '35'),
(24, '45', 'Auxiliatura titular', '10'),
(25, '45', 'Promedio ultima gestion', '30'),
(26, '46', 'Promedio general de las materias cursadas, Incluye', '35'),
(27, '46', 'Promedio general de materias en el periodo académi', '30'),
(28, '47', 'Promedio general de las materias', '35'),
(29, '49', 'none', '65'),
(32, '50', 'rendimiento de pruena', '20'),
(33, '50', 'dsad', '20'),
(34, '50', 'dsad', '10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `requisitoobligatorio`
--

DROP TABLE IF EXISTS `requisitoobligatorio`;
CREATE TABLE `requisitoobligatorio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `requisitoobligatorio`
--

INSERT INTO `requisitoobligatorio` (`id`, `nombre`) VALUES
(1, 'Solicitud de auxiliatura'),
(2, 'Kardex'),
(3, 'Certificado de estudiante regular'),
(4, 'Fotocopia de carnet de identidad '),
(5, 'Certificado de no adeudado a la Biblioteca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `requisitoopcional`
--

DROP TABLE IF EXISTS `requisitoopcional`;
CREATE TABLE `requisitoopcional` (
  `id` int(11) NOT NULL,
  `id_conv` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `punto` int(11) NOT NULL,
  `punto_max` int(11) NOT NULL,
  `nombre_clave` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `requisitoopcional`
--

INSERT INTO `requisitoopcional` (`id`, `id_conv`, `nombre`, `punto`, `punto_max`, `nombre_clave`) VALUES
(12, 21, 'Auxiliar de Laboratorio ', 2, 4, NULL),
(14, 26, 'Auxiliar de Laboratorio', 2, 6, NULL),
(15, 26, 'Auxiliar de mantenimiento', 2, 2, NULL),
(16, 26, 'Auxiliar AdHonorem', 1, 3, NULL),
(19, 21, 'Auxiliar de Mantenimiento', 2, 6, NULL),
(20, 21, 'Auxiliar de Desarrolo ', 2, 6, NULL),
(21, 21, 'Auxiliar AdHonorem', 1, 3, NULL),
(22, 41, 'Experiencia General Prueba Ariel', 2, 6, NULL),
(23, 21, 'Administrador Invitado ', 2, 16, NULL),
(24, 44, 'Requisito de prueba ', 2, 6, NULL),
(25, 45, 'Certificados extrauniversitarios', 1, 2, NULL),
(26, 45, 'Prueba de documentos ', 4, 16, NULL),
(27, 45, 'puntos', 4, 15, NULL),
(28, 46, 'Auxiliares de Practicas Laboratorio Departamento de Informática y Sistemas', 1, 6, NULL),
(29, 46, 'Otros auxiliares en laboratorios de computación', 1, 2, NULL),
(30, 47, 'Auxiliar de Laboratorio Departamento de Informática', 2, 12, NULL),
(31, 49, 'none', 2, 20, NULL),
(32, 50, 'Auxiliar de Laboratorio Departamento de Informática - Sistemas del item respectivo ', 2, 12, NULL),
(33, 50, 'Auxiliares de Practicas Laboratorio Departamento de Informática -  Sistemas ', 1, 6, NULL),
(34, 50, 'sdsd', 2, 20, NULL),
(35, 50, 'dsds', 1, 7, NULL),
(36, 50, 'wqewqe', 2, 5, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_comision`
--

DROP TABLE IF EXISTS `rol_comision`;
CREATE TABLE `rol_comision` (
  `id` int(8) NOT NULL,
  `nombre_rol` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol_comision`
--

INSERT INTO `rol_comision` (`id`, `nombre_rol`) VALUES
(1, 'Meritos'),
(2, 'Conocimiento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion_merito`
--

DROP TABLE IF EXISTS `seccion_merito`;
CREATE TABLE `seccion_merito` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `seccion_merito`
--

INSERT INTO `seccion_merito` (`id`, `nombre`) VALUES
(1, 'Rendimiento Academico'),
(2, 'Experiencia General');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tematica`
--

DROP TABLE IF EXISTS `tematica`;
CREATE TABLE `tematica` (
  `id` int(8) NOT NULL,
  `id_item` varchar(50) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `porcentaje` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tematica`
--

INSERT INTO `tematica` (`id`, `id_item`, `nombre`, `porcentaje`) VALUES
(7, '11', 'ENSAMBLAJE Y MANTENIMIENTO DE COMPUTADORA EN HARDWARE Y SOFTWARE ', '20'),
(8, '11', 'POSTGRES, MYSQL NIVEL INTERMEDIO', '20'),
(12, '43', '2', '2'),
(13, '21', 'Tematica Prueba Ariel ', '30'),
(14, '13', 'Prueba de Ariel ', '10'),
(15, '14', 'Didactica', '10'),
(16, '22', 'DM LINUX ', '25'),
(17, '22', 'EDES NIVEL INTERMEDIO ', '25'),
(18, '22', 'NSAMBLAJE Y MANTENIMIENTO DE COMPUTADORA EN HARDWARE Y SOFTWARE', '20'),
(19, '22', 'IDÁCTICA', '10'),
(20, '22', 'POSTGRES, MYSQL NIVEL INTERMEDIO', '20'),
(22, '23', 'DM LINUX  ', '25'),
(23, '23', 'OSTGRES, MYSQL NIVEL INTERMEDIO', '30'),
(25, '24', 'Admin Servidores', '50'),
(26, '24', 'Mantenimiento', '50'),
(27, '25', 'Linu', '25'),
(28, '25', 'Programacion ', '30'),
(29, '28', 'REDES NIVEL INTERMEDIO', '25'),
(30, '28', 'ENSAMBLAJE Y MANTENIMIENTO DE COMPUTADORA EN HARDWARE Y SOFTWARE', '20'),
(31, '28', 'DIDÁCTICA', '10'),
(32, '28', 'POSTGRES, MYSQL NIVEL INTERMEDIO', '20'),
(33, '28', 'ADM LINUX', '25'),
(35, '11', 'prueba', '100'),
(36, '30', 'ADM LINUX ', '25'),
(37, '30', 'REDES NIVEL INTERMEDIO ', '75'),
(38, '31', 'ADM LINUX  ', '25'),
(39, '31', 'REDES NIVEL INTERMEDIO ', '25'),
(40, '31', 'POSTGRES, MYSQL NIVEL INTERMEDIO', '20'),
(41, '31', 'ENSAMBLAJE Y MANTENIMIENTO DE COMPUTADORA EN HARDWARE Y SOFTWARE ', '20'),
(42, '31', 'DIDÁCTICA', '10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `user_name` varchar(15) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_namep` varchar(20) DEFAULT NULL,
  `last_namem` varchar(20) DEFAULT NULL,
  `password` varchar(8) DEFAULT NULL,
  `facultad` varchar(200) DEFAULT NULL,
  `carrera` varchar(200) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `phone` varchar(8) DEFAULT NULL,
  `dir` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `tipo`, `user_name`, `first_name`, `last_namep`, `last_namem`, `password`, `facultad`, `carrera`, `email`, `phone`, `dir`) VALUES
(49, 'Secretaria', 'secretaria', 'Maria', 'Perez', 'Salvatierra', '12345678', '', '', 'secretaria@algo.com', '34567890', 's/n'),
(50, 'Postulante', 'postulante', 'Roberto', 'Gomez', 'Bolaños', '12345678', '', '', 'postulante@algo.com', '23456789', 's/n'),
(51, 'Administrador', 'admin', 'Juan', 'Perez', 'Gomez', '12345678', '', '', 'admin@algo.com', '12345678', 's/n'),
(59, 'Comision', 'comision ', 'Alberto Ariel Andia ', NULL, NULL, '12345678', NULL, NULL, '', NULL, NULL),
(60, 'Comision', 'comision dos', 'Miguel Angel Molina', NULL, NULL, '12345678', NULL, NULL, '', NULL, NULL),
(61, 'Comision', 'comsion2', 'Mayra Choque', NULL, NULL, '12345678', NULL, NULL, '', NULL, NULL),
(62, 'Comision', 'comision tres', 'Jose Caero', NULL, NULL, '12345678', NULL, NULL, '', NULL, NULL),
(63, 'Postulante', 'postulante2', 'Alberto Ariel', 'Andia ', 'Rodriguez', '123456', NULL, NULL, NULL, NULL, NULL),
(64, 'Postulante', 'mayra', 'Mayra Flora', 'Choque', 'Mayta', '12345678', NULL, NULL, NULL, NULL, NULL),
(65, 'Postulante', 'miguel', 'Miguel Angel ', 'Molina ', 'Arnez', '12345678', NULL, NULL, NULL, NULL, NULL),
(66, 'Postulante', 'jose', 'Jose Israel ', 'Caero', 'Teran', '12345678', NULL, NULL, NULL, NULL, NULL),
(67, 'Postulante', 'ruth', 'Ruth Vanesa ', 'Rocha', 'Pedro', '12345678', NULL, NULL, NULL, NULL, NULL),
(68, 'Secretaria', 'secretaria2', 'Mayra Flora', 'Choque', 'Mayta', '123456', NULL, NULL, NULL, NULL, NULL),
(69, 'Administrador', 'admin2', 'Miguel Angel', 'Molina ', 'Arnez', '123456', NULL, NULL, NULL, NULL, NULL),
(71, 'Comision', 'comisionruth', 'ruth rocha', NULL, NULL, '12345678', NULL, NULL, '', NULL, NULL),
(72, 'Postulante', 'ab', 'ab', 'abn', 'abc', '123456', ' Ciencias y Tecnologia', 'Sistemas', 'ab@gmail.com', '12345678', '123456'),
(73, 'Postulante', 'ab', 'abc', 'abc', 'abc', '123456', ' Ciencias y Tecnologia', 'Sistemas', 'ab@gmail', '12345678', '123456'),
(74, 'Postulante', 'prueba', 'ru', 'ap', 'ap', '123456', ' Ciencias y Tecnologia', 'Informatica', 'naturexs@58gmail.com', '63885385', '36jn+98'),
(75, 'Comision', 'comision5', 'vanesa rocha', NULL, NULL, '12345678', NULL, NULL, '', NULL, NULL),
(76, 'Postulante', 'MiguelA', 'Miguel Angel', 'Molina', 'Arnez', '12345678', ' Ciencias y Tecnologia', 'Sistemas', 'mg@gmail.com', '74369878', 'Av. Salamanca'),
(77, 'Comision', 'MigueA', 'Miguel Angel', NULL, NULL, '12345678', NULL, NULL, '', NULL, NULL),
(78, 'Postulante', 'prueba500', 'prueba', 'ggg', 'ggg', '12345678', ' Ciencias y Tecnologia', 'Sistemas', 'hola@gmail.com', '76853645', '123 avenida'),
(79, 'Comision', 'comision6', 'Luis Rojas', NULL, NULL, '12345678', NULL, NULL, '', NULL, NULL),
(80, 'Postulante', 'PostulanteExame', 'PostulanteExamen', 'Postulante', 'Postulante', '12345678', ' Ciencias y Tecnologia', 'Informatica', 'algo@gmail.com', '12345678', 'noness'),
(81, 'Postulante', 'Adan', 'Adan', 'Vargas', 'Valles', '123456', ' Ciencias y Tecnologia', 'Sistemas', 'adanvv@gmail.com', '34534673', 'Cercado');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agregarrequisito`
--
ALTER TABLE `agregarrequisito`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `codigo`
--
ALTER TABLE `codigo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `comision`
--
ALTER TABLE `comision`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `convocatoria`
--
ALTER TABLE `convocatoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `fileuploads`
--
ALTER TABLE `fileuploads`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inscripcionconvocatoria`
--
ALTER TABLE `inscripcionconvocatoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inscripcion_item`
--
ALTER TABLE `inscripcion_item`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lista_habilitados`
--
ALTER TABLE `lista_habilitados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `meritos_seccion`
--
ALTER TABLE `meritos_seccion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nota_rendimiento`
--
ALTER TABLE `nota_rendimiento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nota_tematica`
--
ALTER TABLE `nota_tematica`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `punto_experiencia`
--
ALTER TABLE `punto_experiencia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rendimiento_academico`
--
ALTER TABLE `rendimiento_academico`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `requisitoobligatorio`
--
ALTER TABLE `requisitoobligatorio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `requisitoopcional`
--
ALTER TABLE `requisitoopcional`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol_comision`
--
ALTER TABLE `rol_comision`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seccion_merito`
--
ALTER TABLE `seccion_merito`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tematica`
--
ALTER TABLE `tematica`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agregarrequisito`
--
ALTER TABLE `agregarrequisito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT de la tabla `codigo`
--
ALTER TABLE `codigo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT de la tabla `comision`
--
ALTER TABLE `comision`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `convocatoria`
--
ALTER TABLE `convocatoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `fileuploads`
--
ALTER TABLE `fileuploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `inscripcionconvocatoria`
--
ALTER TABLE `inscripcionconvocatoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- AUTO_INCREMENT de la tabla `inscripcion_item`
--
ALTER TABLE `inscripcion_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `lista_habilitados`
--
ALTER TABLE `lista_habilitados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `meritos_seccion`
--
ALTER TABLE `meritos_seccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `nota_rendimiento`
--
ALTER TABLE `nota_rendimiento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `nota_tematica`
--
ALTER TABLE `nota_tematica`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `punto_experiencia`
--
ALTER TABLE `punto_experiencia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `rendimiento_academico`
--
ALTER TABLE `rendimiento_academico`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `requisitoobligatorio`
--
ALTER TABLE `requisitoobligatorio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `requisitoopcional`
--
ALTER TABLE `requisitoopcional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `rol_comision`
--
ALTER TABLE `rol_comision`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `seccion_merito`
--
ALTER TABLE `seccion_merito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tematica`
--
ALTER TABLE `tematica`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

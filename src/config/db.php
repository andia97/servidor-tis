<?php
  class db {
    private $dbHost = 'localhost';
    private $dbUser = 'admin';
    private $dbPass = 'admin';
    private $dbName = 'tis';

    public function connectDB() {
      $mysqlConnect = "mysql:host=$this->dbHost;dbname=$this->dbName";
      $dbConnect = new PDO($mysqlConnect, $this->dbUser, $this->dbPass);
      $dbConnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      return $dbConnect;
    }
  }

<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/*$app->get('/', function (Request $request, Response $response, $args) {
    return $response->getBody()->write("API Works.");
  });*/

//La secretaria solicita un codigo
$app->get('/codigo/{typeUser}', function (Request $request, Response $response) {

  $tipo_de_usuario = $request->getAttribute('typeUser');

  $codigo = generarCodigo();
  $sql = "INSERT INTO codigo (codigo, tipo) VALUES (:codigo, :tipo)";

  try {
    $db = new db();
    $db = $db -> connectDB();
    $resultado = $db -> prepare($sql);
    $resultado -> bindParam(':codigo', $codigo);
    $resultado -> bindParam(':tipo', $tipo_de_usuario);
    $resultado -> execute();

    $resultado = null;
    $db = null;
    return $response->getBody()->write(json_encode($codigo));
  } catch(PDOException $e) {
    echo '{"error" : {"text":'.$e->getMessage().'}';
  }
});

//Recibimos el Codigo de el frontend y comparamos si existe y si esta activo
$app->post('/codigo', function (Request $request, Response $response) {
  $codigo = $request->getParam('codigo');
  $sql = "SELECT tipo,estado FROM codigo WHERE codigo='$codigo'";

  try {
    $db = new db();
    $db = $db->connectDB();
    $resultado = $db->query($sql);

    if($resultado->rowCount() > 0) {
      $row = $resultado->fetchAll(PDO::FETCH_OBJ);
      switch($row[0]->estado) {
        case "Activo": $response->getBody()->write(json_encode($row[0])); break;
        case "Usado": $response->getBody()->write(json_encode("El codigo que ha ingresado ya ha sido usado.")); break;
      }
      return $response;
    } else {
      return $response->getBody()->write(json_encode("El codigo ingresado no existe, verifique e intente nuevamente."));
    }

    $estado = null;
    $row = null;
    $resultado = null;
    $db = null;
  } catch(PDOException $e) {
    echo '{"error" : {"text":'.$e->getMessage().'}';
  }
});

function generarCodigo() {

  $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $random_string = '';

  for( $i=0; $i < 5; $i++ ) {
    $random_character = $caracteres_permitidos[mt_rand(0, strlen($caracteres_permitidos) - 1)];
    $random_string .= $random_character;
  }

  return $random_string;
}
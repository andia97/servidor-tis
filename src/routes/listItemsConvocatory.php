<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get("/listItemsConvocatory/{idConv}", function (Request $request, Response $response, $args=[]) {
    $idConv = $request->getAttribute("idConv");
    $sql = "SELECT * FROM item WHERE id_conv='$idConv'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $items = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($items));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
$app->get("/item/{id}", function (Request $request, Response $response, $args=[]) {
    $idItem = $request->getAttribute("id");
    $sql = "SELECT * FROM item WHERE id='$idItem'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $items = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($items));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get("/listarItemesUsuario/{idUser}/{idConv}", function (Request $request, Response $response, $args=[]) {
    $idConv = $request->getAttribute("idConv");
    $idUser = $request->getAttribute("idUser");
    $sql = "SELECT I.id_user,I.id_item,I.id_conv,It.nombre_item
            FROM inscripcion_item AS I, item AS It
            WHERE I.id_conv='$idConv' AND I.id_user='$idUser' AND I.id_item=It.id ";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $items = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($items));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
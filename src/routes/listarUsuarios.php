<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/usuarios', function (Request $request, Response $response, $args) {

    $sql = "SELECT * FROM usuario";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $usuarios = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($usuarios));
        }else{
            $response->getBody()->write(json_encode("No existen usuarios registrados"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
$app->get('/usuariosComision', function (Request $request, Response $response, $args) {

    $sql = "SELECT * FROM usuario WHERE tipo='Comision'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $usuarios = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($usuarios));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
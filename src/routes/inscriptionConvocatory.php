<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->post('/inscriptionConvocatory', function (Request $request, Response $response, $args) {

    $newInscriptionConvocatory = json_decode($request->getBody());
    $idConv = $newInscriptionConvocatory->idConv;
    $idUser = $newInscriptionConvocatory->idUser;
    $sql = "INSERT INTO inscripcionconvocatoria (id, id_user, id_conv)
            VALUES (NULL,:id_user,:id_conv)";
    try{
        $db = new db();
        $db = $db->connectDB();
        if(existePostul($idConv,$idUser,$db) == 0){
            $insert = $db->prepare($sql);
            $insert->bindParam("id_user",$newInscriptionConvocatory->idUser);
            $insert->bindParam("id_conv",$newInscriptionConvocatory->idConv);
            $insert->execute();
            $response->getBody()->write(json_encode("Succes"));
        }else{
            $response->getBody()->write(json_encode("1"));//Ya esta participando en la convocatoria
        }
           
        return $response;
        $db= null;

    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
function existePostul($idConv,$idUser,$conexion){
    $sql = "SELECT *
            FROM inscripcionconvocatoria 
            WHERE id_user='$idUser' And id_conv='$idConv'";
    $result = $conexion->query($sql);
    $tamaño = $result->rowCount();
    if($tamaño > 0){
        return 1;
    }else{
        return 0;
    }
}
$app->post('/deleteInscription', function (Request $request, Response $response, $args) {

    $newDelForm = json_decode($request->getBody());
    $id_conv = $newDelForm->idConv;
    $id_user = $newDelForm->idUser;
    $sql = $sql = "DELETE FROM inscripcionconvocatoria
                    WHERE id_conv='$id_conv' and id_user='$id_user'";
    try{
    $db = new db();
    $db = $db->connectDB();
        $delete = $db->prepare($sql);
        $delete->bindParam("id_conv",$newDelForm->idConv);
        $delete->bindParam("id_user",$newDelForm->idUser);
        $delete->execute();
        $response->getBody()->write(json_encode("Succes Delete"));
    return $response;
    $db= null;
    }catch(PDOException $e){
    echo $e->getMessage();
    }
});
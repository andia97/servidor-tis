<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app->post('/getidinscconv', function (Request $request, Response $response) {
  $userId = $request->getParam('idUser');
  $convId = $request->getParam('idConv');
  $sql = "SELECT id FROM inscripcionconvocatoria WHERE id_user='$userId' AND id_conv='$convId'";

  try {
    $db = new db();
    $db = $db->connectDB();
    $resultado = $db->query($sql);

    if($resultado->rowCount() > 0) {
      $row = $resultado->fetchAll(PDO::FETCH_OBJ);
      return $response->getBody()->write(json_encode($row));
    } else {
      return $response->getBody()->write(json_encode("Error al procesar."));
    }

    $row = null;
    $resultado = null;
    $db = null;
  } catch(PDOException $e) {
    echo '{"error" : {"text":'.$e->getMessage().'}';
  }
});

$app->post('/setFileDir', function (Request $request, Response $response) {
  $id_insc_conv = $request->getParam('id_insc_conv');
  $nameReq = $request->getParam('nameReq');
  $file_dir = $request->getParam('file_dir');
  $tipo = $request->getParam('tipo');
  $estado = $request->getParam('estado');
  $observacion = $request->getParam('observacion');
  $puntuacion = $request->getParam('puntuacion');
  $sql = "INSERT INTO fileuploads (id_insc_conv, name_req, file_dir, tipo, estado, observacion, puntuacion) VALUES (:id_insc_conv, :name_req, :file_dir, :tipo, :estado, :observacion, :puntuacion)";

  try {
    $db = new db();
    $db = $db -> connectDB();
    $resultado = $db -> prepare($sql);
    $resultado -> bindParam(':id_insc_conv', $id_insc_conv);
    $resultado -> bindParam(':name_req', $nameReq);
    $resultado -> bindParam(':file_dir', $file_dir);
    $resultado -> bindParam(':tipo', $tipo);
    $resultado -> bindParam(':estado', $estado);
    $resultado -> bindParam(':observacion', $observacion);
    $resultado -> bindParam(':puntuacion', $puntuacion);
    $resultado -> execute();

    $resultado = null;
    $db = null;
    return $response->getBody()->write(json_encode("Archivos guardados exitosamente"));
  } catch(PDOException $e) {
    echo '{"error" : {"text":'.$e->getMessage().'}';
  }
});


$app->post('/actualizarEstadoDocumento', function (Request $request, Response $response, $args) {

  $newFile = json_decode($request->getBody());
  $idFile = $newFile->idFile;
  $sql = "UPDATE fileuploads
          SET estado=:estado
          WHERE id='$idFile'";
  try{
      $db = new db();
      $db = $db->connectDB();
          $insert = $db->prepare($sql);
          $insert->bindParam("estado",$newFile->estado);
          $insert->execute();
          $response->getBody()->write(json_encode("Succes"));
      return $response;
      $db= null;

  }catch(PDOException $e){
      echo $e->getMessage();
  }
});


$app->get('/obtenerIdInscri/{idUser}/{idConv}', function (Request $request, Response $response, $args) {
  $idUser = $request->getAttribute("idUser");
  $idConvocatoria = $request->getAttribute("idConv");
  $sql = "SELECT *
          FROM inscripcionconvocatoria
          WHERE id_conv='$idConvocatoria' AND id_user='$idUser'";
  try{
      $db = new db();
      $db = $db->connectDB();
      $resultado = $db->query($sql);
      $tam = $resultado->rowCount();
      if($tam > 0){
          $convocatoria = $resultado->fetchAll(PDO::FETCH_OBJ);
          $response->getBody()->write(json_encode($convocatoria));
      }else{
          $response->getBody()->write(json_encode("Empty"));
      }
      return $response;
  }catch(PDOException $e){
      echo $e->getMessage();
  }
});


$app->post('/actualizarPuntosEXP', function (Request $request, Response $response, $args) {

  $newFile = json_decode($request->getBody());
  $idFile = $newFile->idFile;
  $puntosModificar = $newFile->puntoAniadir;
  $puntosDocumento = $newFile->puntoDoc;
  $sql = "UPDATE fileuploads
          SET puntuacion=:puntuacion
          WHERE id='$idFile'";
  try{      
    
      $db = new db();
      $db = $db->connectDB();
      if(verificarPuntosExGeneral($puntosDocumento,$puntosModificar)){
          $insert = $db->prepare($sql);
          $insert->bindParam("puntuacion",$newFile->puntoAniadir);
          $insert->execute();
          $response->getBody()->write(json_encode("Succes"));
      }else{
          $response->getBody()->write(json_encode("Fail"));
      }

      return $response;
      $db= null;

  }catch(PDOException $e){
      echo $e->getMessage();
  }
});

function verificarPuntosExGeneral($puntosDocumento,$puntosModificar){
    
        if($puntosModificar == $puntosDocumento || $puntosModificar == 0){
            return true;
        }else{
            return false;
        }
}
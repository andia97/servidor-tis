<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/listarRolComision', function (Request $request, Response $response, $args) {

    $sql = "SELECT * FROM rol_comision";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $reqObligatorios = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($reqObligatorios));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
$app->post('/registrarRolUsuario', function (Request $request, Response $response, $args) {

    $newRolForm = json_decode($request->getBody());
    $idUser = $newRolForm->idUser;
    $idRol = $newRolForm->idRol;
    $idConv = $newRolForm->idConv;
    
    $sql = "INSERT INTO comision (id, id_user, id_conv, id_rol) 
    VALUES (NULL,:id_user,:id_conv,:id_rol)";
    try{
    $db = new db();
    $db = $db->connectDB();
    if(existeRolComision($idUser,$idConv,$idRol,$db) == 0){
        $insert = $db->prepare($sql);
        $insert->bindParam("id_user",$newRolForm->idUser);
        $insert->bindParam("id_conv",$newRolForm->idConv);
        $insert->bindParam("id_rol",$newRolForm->idRol);
        $insert->execute();
        $response->getBody()->write(json_encode("Succes"));
    }else{
        $response->getBody()->write(json_encode("1"));//ya registrado
    }
    return $response;
    $db= null;

    }catch(PDOException $e){
    echo $e->getMessage();
    }

});
function existeRolComision($idUser,$idConv,$idRol,$conexion){
    $sql = "SELECT * 
            FROM comision
            WHERE id_user = '$idUser' AND id_rol = '$idRol' AND id_conv='$idConv'";
    $result = $conexion->query($sql);
    $tamaño = $result->rowCount();
    if($tamaño > 0){
        return 1;
    }else{
        return 0;
    }
}

$app->get('/listarConvocatoriasComision/{idUser}', function (Request $request, Response $response, $args) {

    $idComision = $request->getAttribute('idUser');
    $sql = "SELECT Conv.id,Conv.nombre_conv, R.nombre_rol
            FROM comision AS C, usuario AS U, rol_comision AS R, convocatoria AS Conv 
            WHERE C.id_user=U.id AND C.id_conv=Conv.id AND C.id_rol=R.id AND U.id='$idComision'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $reqObligatorios = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($reqObligatorios));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
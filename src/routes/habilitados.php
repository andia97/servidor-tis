<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->post('/habilitarPostulante', function (Request $request, Response $response, $args) {

    $newValidarForm = json_decode($request->getBody());
    $idUsuario = $newValidarForm->idUser;
    $idConvocatoria =$newValidarForm->idConv;
    
    $sql = "INSERT INTO lista_habilitados (id, id_user, id_conv,estado)
            VALUES (NULL, :id_user, :id_conv, :estado)";
    try{
        $db = new db();
        $db = $db->connectDB();
        if(existePostulante($idUsuario,$idConvocatoria,$db) == 0){
            $insert = $db->prepare($sql);
            $insert->bindParam("id_user",$newValidarForm->idUser);
            $insert->bindParam("id_conv",$newValidarForm->idConv);
            $insert->bindParam("estado",$newValidarForm->estado);
            $insert->execute();
            $response->getBody()->write(json_encode("Succes"));
        }else{
            $response->getBody()->write(json_encode("1"));//usuario ya registrado

        }
        return $response;
        $db= null;

    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
function existePostulante($idUser,$idConvocatoria,$conexion){
    $sql = "SELECT * 
            FROM lista_habilitados
            WHERE id_user = '$idUser' AND id_conv ='$idConvocatoria'";
    $result = $conexion->query($sql);
    $tamaño = $result->rowCount();
    if($tamaño > 0){
        return 1;
    }else{
        return 0;
    }
}
$app->get('/listaHabilitados/{idConv}', function (Request $request, Response $response, $args) {
    $oneConv = $request->getAttribute("idConv");
    $sql = "SELECT U.id,H.id_conv,U.first_name,U.last_namep,U.last_namem 
            FROM lista_habilitados as H, usuario as U 
            WHERE id_conv='$oneConv' AND estado='true' AND H.id_user=U.id";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $convocatoria = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($convocatoria));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
$app->get('/listaInhabilitados/{idConv}', function (Request $request, Response $response, $args) {
    $oneConv = $request->getAttribute("idConv");
    $sql = "SELECT U.id,H.id_conv,U.first_name,U.last_namep,U.last_namem 
            FROM lista_habilitados as H, usuario as U 
            WHERE id_conv='$oneConv' AND estado='false' AND H.id_user=U.id";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $convocatoria = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($convocatoria));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->post('/addReqOpt', function (Request $request, Response $response, $args) {

    $newReqOption = json_decode($request->getBody());
    $idConvo=$newReqOption->idConv;
    $puntoParaAñadir=$newReqOption->pointMax;
    $idSeccion=$newReqOption->idSeccion;
    $sql = "INSERT INTO requisitoopcional (id, id_conv, nombre, punto, punto_max)
            VALUES (NULL,:id_conv,:nombre,:punto,:punto_max)";
    try{
    $db = new db();
    $db = $db->connectDB();
    switch(verificarPuntosExperienciaConvocatoria($idConvo,$puntoParaAñadir,$idSeccion,$db)){
        case 1:
            $insert = $db->prepare($sql);
            $insert->bindParam("id_conv",$newReqOption->idConv);
            $insert->bindParam("nombre",$newReqOption->name);
            $insert->bindParam("punto",$newReqOption->point);
            $insert->bindParam("punto_max",$newReqOption->pointMax);
            $insert->execute();
            $response->getBody()->write(json_encode("Succes"));
        break;
        case 2:
            $insert = $db->prepare($sql);
            $insert->bindParam("id_conv",$newReqOption->idConv);
            $insert->bindParam("nombre",$newReqOption->name);
            $insert->bindParam("punto",$newReqOption->point);
            $insert->bindParam("punto_max",$newReqOption->pointMax);
            $insert->execute();
            $response->getBody()->write(json_encode("Succes"));
        break;
        case 3:
            $response->getBody()->write(json_encode("Fail")); 
        break;
    }
    return $response;
    $db= null;

    }catch(PDOException $e){
    echo $e->getMessage();
    }
});

function verificarPuntosExperienciaConvocatoria($idConvo,$puntoParaAñadir,$idSeccion,$db){
    $sql2 ="SELECT SUM(punto_max) as puntosAcumulados FROM requisitoopcional WHERE id_conv='$idConvo'";
    $resultado = $db->query($sql2);
    $tam = $resultado->rowCount();
    $puntosAsignadosSeccion = obtenerPuntosSeccion($idConvo,$idSeccion,$db);
    if($tam > 0){
        $seccion = $resultado->fetchAll(PDO::FETCH_COLUMN);
        $puntosAcumulados = $seccion[0];
        if($puntosAcumulados < $puntosAsignadosSeccion){
            $sumaDePuntos= $puntoParaAñadir+$puntosAcumulados;
            if($sumaDePuntos <= $puntosAsignadosSeccion ){
                //print("Puntos para añadir:".$puntoParaAñadir);
                return 1;            
            }else{
                //print("No se puede añadir, la suma es:".$sumaDePuntos);
                return 3;
            }
        }else{
            return 3;
        }
    }else{
        return 2;
    }
}

function obtenerPuntosSeccion($idConvo,$idSeccion,$db){
    $sql = "SELECT puntos FROM meritos_seccion WHERE id_conv='$idConvo' AND id_seccion= '$idSeccion'";
    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $suma = $resultado->fetchAll(PDO::FETCH_COLUMN);
            $puntos = $suma[0];
            return $puntos;
        }else{
            return 0;
        }
    }catch(PDOException $e){
        echo $e->getMessage();
    }

}


$app->get("/sumaExperiencia/{idConv}", function (Request $request, Response $response, $args=[]) {
    $oneConv = $request->getAttribute("idConv");
    $sql = "SELECT SUM(punto_max) as puntosAcumulados FROM requisitoopcional WHERE id_conv='$oneConv'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $suma = $resultado->fetchAll(PDO::FETCH_COLUMN);
            $response->getBody()->write(json_encode($suma));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get("/listReqConvocatory/{idConv}", function (Request $request, Response $response, $args=[]) {
    $idConv = $request->getAttribute("idConv");
    $sql = "SELECT * FROM requisitoopcional WHERE id_conv='$idConv'";
    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $convocatorias = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($convocatorias));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->post('/deleteReqOpt', function (Request $request, Response $response, $args) {

    $idReqOption = json_decode($request->getBody());
    $idOption = $idReqOption->idReq;
    $sql = $sql = "DELETE FROM requisitoopcional
                    WHERE id='$idOption'";
    try{
    $db = new db();
    $db = $db->connectDB();
        $delete = $db->prepare($sql);
        $delete->bindParam("id",$idReqOption->idReq);
        $delete->execute();
        $response->getBody()->write(json_encode("Succes Delete"));
    return $response;
    $db= null;
    }catch(PDOException $e){
    echo $e->getMessage();
    }
});

//La comision Asigna Puntos Por Requisito 
$app->post('/aniadirPuntosExperiencia', function (Request $request, Response $response, $args) {

    $newRendimiento = json_decode($request->getBody());
    $idReq=$newRendimiento->idReq;
    $puntosParaAñadir=$newRendimiento->punto;
    $idUser=$newRendimiento->idUser;
    $sql = "INSERT INTO punto_experiencia (id, id_user, id_req, id_conv, punto)
            VALUES (NULL,:id_user,:id_req, :id_conv,:punto)";
    try{
    $db = new db();
    $db = $db->connectDB();
    if(existeUsuarioConExperiencia($idReq,$idUser,$db) == 0){
        switch(verificarPuntosExperiencia($idReq,$puntosParaAñadir,$db)){
            case 1:
                $insert = $db->prepare($sql);
                $insert->bindParam("id_user",$newRendimiento->idUser);
                $insert->bindParam("id_req",$newRendimiento->idReq);
                $insert->bindParam("id_conv",$newRendimiento->idConv);
                $insert->bindParam("punto",$newRendimiento->punto);
                $insert->execute();
                $response->getBody()->write(json_encode("Succes"));
            break;
            case 2:
                $response->getBody()->write(json_encode("Fail")); 
            break;
            case 3:
                $response->getBody()->write(json_encode("Empty")); 
            break;
            case 4:
                $response->getBody()->write(json_encode("Max")); 
            break;
        }
    }else{
        $response->getBody()->write(json_encode("1"));//usuario ya registrado
    }
    return $response;
    $db= null;

    }catch(PDOException $e){
    echo $e->getMessage();
    }
});
function existeUsuarioConExperiencia($idReq,$idUser,$conexion){
    $sql = "SELECT * 
            FROM punto_experiencia
            WHERE id_req = '$idReq' AND id_user='$idUser'";
    $result = $conexion->query($sql);
    $tamaño = $result->rowCount();
    if($tamaño > 0){
        return 1;
    }else{
        return 0;
    }
}
function verificarPuntosExperiencia($idReq,$puntosParaAñadir,$db){
    $sql2 ="SELECT punto FROM requisitoopcional WHERE id='$idReq'";
    $resultado = $db->query($sql2);
    $tam = $resultado->rowCount();
    if($tam > 0){
        $puntoPorDocumento = $resultado->fetchAll(PDO::FETCH_COLUMN);
        $puntoPermitido = $puntoPorDocumento[0];
        if($puntosParaAñadir>=$puntoPermitido && $puntosParaAñadir%$puntoPermitido == 0){
            switch(verificarPuntoMaximoExperiencia($idReq,$puntosParaAñadir,$db)){
                case 1: 
                    return 1;
                break;
                case 2: 
                    return 4; //El punto para añidir es mayor a la maxima permitida
                break; 
                case 3:
                    return 3; // No existe datos cone el idReq suministrado
                break;
            }       
        }else{
            return 2; //El punto para añadir no coicide con el punto permitido por documento
        }
    }else{
        return 3; //EL postulante ya fue evaluado en el requerimiento con idReq
    }
}
function verificarPuntoMaximoExperiencia($idReq,$puntosParaAñadir,$db){
    $sql3 ="SELECT punto_max FROM requisitoopcional WHERE id='$idReq'";
    $resultado = $db->query($sql3);
    $tam = $resultado->rowCount();
    if($tam > 0){
        $puntoMaximoAcumulable = $resultado->fetchAll(PDO::FETCH_COLUMN);
        $puntoMaximo = $puntoMaximoAcumulable[0];
        if($puntosParaAñadir <= $puntoMaximo ){
            return 1;
        }else{
            return 2;
        }
    }else{
        return 3;
    }
}


$app->get("/listarPuntosExperiencia/{idUser}/{idConv}", function (Request $request, Response $response, $args=[]) {
    $user = $request->getAttribute("idUser");
    $conv = $request->getAttribute("idConv");
    $sql = "SELECT R.nombre,R.punto_max,P.punto
            FROM punto_experiencia AS P, requisitoopcional AS R
            WHERE P.id_user='$user' AND P.id_conv='$conv' AND P.id_req=R.id AND P.id_conv=R.id_conv";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $notaUser = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($notaUser));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get("/sumarPuntos/{idInscri}", function (Request $request, Response $response, $args=[]) {
    $idInscri = $request->getAttribute("idInscri");
    $sql = "SELECT SUM(puntuacion) as puntoTotal FROM fileuploads WHERE id_insc_conv='$idInscri'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $suma = $resultado->fetchAll(PDO::FETCH_COLUMN);
            $response->getBody()->write(json_encode($suma));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});


$app->get('/listarRequisitosOpcional/{idInscrip}', function (Request $request, Response $response, $args) {
    $idInscripcion = $request->getAttribute("idInscrip");
    $sql = "SELECT DISTINCT F.id AS idFile, F.id_insc_conv, F.name_req, F.file_dir,F.puntuacion,SUM(puntuacion)AS puntuacionTotal , R.punto,R.punto_max 
            FROM inscripcionconvocatoria AS I, fileuploads AS F, requisitoopcional AS R 
            WHERE I.id=F.id_insc_conv AND F.id_insc_conv='$idInscripcion' AND F.name_req=R.nombre AND F.tipo='opcional' 
            GROUP BY F.name_req 
            ORDER BY puntuacion";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $reqObligatorios = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($reqObligatorios));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get('/listarRequisitosOpcionalPresentados/{idInscrip}/{nameReq}', function (Request $request, Response $response, $args) {
    $idInscripcion = $request->getAttribute("idInscrip");
    $nameReq = $request->getAttribute("nameReq");
    $sql = "SELECT DISTINCT F.id AS idFile, F.id_insc_conv, F.name_req, F.file_dir,F.puntuacion,R.punto,R.punto_max 
            FROM inscripcionconvocatoria AS I, fileuploads AS F, requisitoopcional AS R 
            WHERE I.id=F.id_insc_conv AND F.id_insc_conv='$idInscripcion' AND F.name_req='$nameReq'  
            AND F.name_req=R.nombre AND F.tipo='opcional' AND F.puntuacion!=0 
            ORDER BY puntuacion";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $reqObligatorios = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($reqObligatorios));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
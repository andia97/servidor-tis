<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->post('/createItem', function (Request $request, Response $response, $args) {

    $newItem = json_decode($request->getBody());
    
    $sql = "INSERT INTO item (id, id_conv, nombre_item, hora_laboral, nro_items)
            VALUES (NULL, :id_conv, :nombre_item, :hora_laboral, :nro_items)";
    try{
        $db = new db();
        $db = $db->connectDB();
            $insert = $db->prepare($sql);
            $insert->bindParam("id_conv",$newItem->idConv);
            $insert->bindParam("nombre_item",$newItem->name);
            $insert->bindParam("hora_laboral",$newItem->hrWork);
            $insert->bindParam("nro_items",$newItem->itRequerid);
            $insert->execute();
            $response->getBody()->write(json_encode("Succes"));
        return $response;
        $db= null;

    }catch(PDOException $e){
        echo $e->getMessage();
    }


});
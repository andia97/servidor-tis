<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->post('/inscripcionItem', function (Request $request, Response $response, $args) {

    $newIncripcionItemForm = json_decode($request->getBody());
    $idUsuario = $newIncripcionItemForm->idUser;
    $idItem = $newIncripcionItemForm->idItem;

    $sql = "INSERT INTO inscripcion_item (id, id_user, id_conv,id_item)
            VALUES (NULL, :id_user, :id_conv, :id_item)";
    try{
        $db = new db();
        $db = $db->connectDB();
        if(existePostulanteItem($idUsuario,$idItem,$db) == 0){
            $insert = $db->prepare($sql);
            $insert->bindParam("id_user",$newIncripcionItemForm->idUser);
            $insert->bindParam("id_conv",$newIncripcionItemForm->idConv);
            $insert->bindParam("id_item",$newIncripcionItemForm->idItem);
            $insert->execute();
            $response->getBody()->write(json_encode("Succes"));
        }else{
            $response->getBody()->write(json_encode("1"));//usuario ya registrado

        }
        return $response;
        $db= null;

    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
function existePostulanteItem($idUser,$idItem,$conexion){
    $sql = "SELECT * 
            FROM inscripcion_item
            WHERE id_user = '$idUser' and id_item = '$idItem'";
    $result = $conexion->query($sql);
    $tamaño = $result->rowCount();
    if($tamaño > 0){
        return 1;
    }else{
        return 0;
    }
}
$app->get('/listarUsuariosItem/{idItem}/{idConv}', function (Request $request, Response $response, $args) {
    $idItem = $request->getAttribute("idItem");
    $idConvocatoria = $request->getAttribute("idConv");
    $sql = "SELECT I.id,I.id_user, I.id_conv, I.id_item,H.estado,U.first_name,U.last_namep, U.last_namem
            FROM inscripcion_item AS I, item As It, lista_habilitados AS H, usuario AS U
            WHERE I.id_item = It.id AND I.id_user = H.id_user AND I.id_user=U.id AND I.id_conv=H.id_conv
                    AND I.id_conv='$idConvocatoria' AND I.id_item='$idItem' AND H.estado='true'
            GROUP BY I.id_user";
    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $convocatoria = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($convocatoria));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
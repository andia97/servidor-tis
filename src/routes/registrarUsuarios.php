<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->post('/registrar', function (Request $request, Response $response, $args) {

    $newuser = json_decode($request->getBody());
    $useName = $newuser->userName;
    $email = $newuser->email;
    
    $sql = "INSERT INTO usuario (id,tipo, user_name, first_name, last_namep, last_namem, password, facultad, carrera, email, phone, dir)
    VALUES (NULL,:tipo ,:username, :firstName,:lastNameP,:lastNameM,:password, :facultad, :carrera, :email,:phone,:dir)";
    try{
    $db = new db();
    $db = $db->connectDB();
    if(existe($useName,$email,$db) == 0){
        $insert = $db->prepare($sql);
        $insert->bindParam("tipo",$newuser->tipo);
        $insert->bindParam("username",$newuser->userName);
        $insert->bindParam("firstName",$newuser->firstName);
        $insert->bindParam("lastNameP",$newuser->lastNameP);
        $insert->bindParam("lastNameM",$newuser->lastNameM);
        $insert->bindParam("password",$newuser->password);
        $insert->bindParam("facultad",$newuser->facultad);
        $insert->bindParam("carrera",$newuser->carrera);
        $insert->bindParam("email",$newuser->email);
        $insert->bindParam("phone",$newuser->phone);
        $insert->bindParam("dir",$newuser->dir);
        $insert->execute();
        $response->getBody()->write(json_encode($newuser));
    }else{
        $response->getBody()->write(json_encode("1"));//usuario ya registrado
    }
        return $response;
        $db= null;

    }catch(PDOException $e){
        echo $e->getMessage();
        echo $useName;
    }


});
function existe($name,$email,$conexion){
    $sql = "SELECT * 
            FROM usuario
            WHERE user_name = '$name' and email = '$email'";
    $result = $conexion->query($sql);
    $tamaño = $result->rowCount();
    if($tamaño > 0){
        return 1;
    }else{
        return 0;
    }
}

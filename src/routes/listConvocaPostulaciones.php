<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get("/listConvoPostulaciones/{idUser}", function (Request $request, Response $response, $args=[]) {
    $idUser = $request->getAttribute("idUser");
    $sql = "SELECT DISTINCT C.nombre_conv,U.user_name, I.id ,C.id,date(C.fecha_actual) as fecha_actual,date(C.fecha_limite) as fecha_limite
            FROM convocatoria as C,usuario as U,inscripcionconvocatoria as I 
            WHERE I.id_conv=C.id AND I.id_user=U.id And I.id_user='$idUser'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $convocatorias = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($convocatorias));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
$app->get("/listUserConvocatory/{idConv}", function (Request $request, Response $response, $args=[]) {
    $idConv = $request->getAttribute("idConv");
    $sql = "SELECT DISTINCT I.id as inscId,U.id as userId, U.first_name, U.last_namep, U.last_namem
            FROM convocatoria as C,usuario as U,inscripcionconvocatoria as I 
            WHERE I.id_conv=C.id AND I.id_user=U.id And I.id_conv='$idConv'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $convocatorias = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($convocatorias));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
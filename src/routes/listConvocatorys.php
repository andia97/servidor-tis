<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/listConvocatorys', function (Request $request, Response $response, $args) {

    $sql = "SELECT C.id,C.nombre_conv,C.facultad,C.carrera,C.departamento,date(C.fecha_actual) as fecha_actual,date(fecha_limite) as fecha_limite
            FROM convocatoria as C";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $convocatorias = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($convocatorias));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get('/listarConvocatoriasVigentes', function (Request $request, Response $response, $args) {

    $sql = "SELECT C.id,C.nombre_conv,C.facultad,C.carrera,C.departamento,date(C.fecha_actual) as fecha_actual,date(fecha_limite) as fecha_limite
            FROM convocatoria as C
            WHERE fecha_limite >= date(CURRENT_DATE())";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $convocatorias = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($convocatorias));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get('/getConvocatory/{idConv}', function (Request $request, Response $response, $args) {
    $oneConv = $request->getAttribute("idConv");
    $sql = "SELECT id,nombre_conv FROM convocatoria WHERE id='$oneConv'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $convocatoria = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($convocatoria));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
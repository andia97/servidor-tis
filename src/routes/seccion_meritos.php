<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;



$app->post('/aniadirSecciones', function (Request $request, Response $response, $args) {

    $newSectionForm = json_decode($request->getBody());
    $idConvo=$newSectionForm->idConv;
    $puntoParaAñadir=$newSectionForm->puntos;
    $idSeccion=$newSectionForm->idSeccion;
    $sql = "INSERT INTO meritos_seccion (id, id_seccion, id_conv, puntos)
            VALUES (NULL,:id_seccion,:id_conv,:puntos)";
    try{
    $db = new db();
    $db = $db->connectDB();
    if(existeConfiguracionMeritos($idConvo,$idSeccion,$db)){
        eliminarConfiguracionMeritos($idConvo,$idSeccion,$db);
        switch(verificarPuntos($idConvo,$puntoParaAñadir,$db)){
            case 1:
                $insert = $db->prepare($sql);
                $insert->bindParam("id_conv",$newSectionForm->idConv);
                $insert->bindParam("id_seccion",$newSectionForm->idSeccion);
                $insert->bindParam("puntos",$newSectionForm->puntos);
                $insert->execute();
                $response->getBody()->write(json_encode("Succes")); 
            break;
            case 2:
                $insert = $db->prepare($sql);
                $insert->bindParam("id_conv",$newSectionForm->idConv);
                $insert->bindParam("id_seccion",$newSectionForm->idSeccion);
                $insert->bindParam("puntos",$newSectionForm->puntos);
                $insert->execute();
                $response->getBody()->write(json_encode("Succes")); 
            break;
            case 3:
                $response->getBody()->write(json_encode("Fail")); 
            break;
        }  
    }else{
        switch(verificarPuntos($idConvo,$puntoParaAñadir,$db)){
            case 1:
                $insert = $db->prepare($sql);
                $insert->bindParam("id_conv",$newSectionForm->idConv);
                $insert->bindParam("id_seccion",$newSectionForm->idSeccion);
                $insert->bindParam("puntos",$newSectionForm->puntos);
                $insert->execute();
                $response->getBody()->write(json_encode("Succes")); 
            break;
            case 2:
                $insert = $db->prepare($sql);
                $insert->bindParam("id_conv",$newSectionForm->idConv);
                $insert->bindParam("id_seccion",$newSectionForm->idSeccion);
                $insert->bindParam("puntos",$newSectionForm->puntos);
                $insert->execute();
                $response->getBody()->write(json_encode("Succes"));  
            break;
            case 3:
                $response->getBody()->write(json_encode("Fail")); 
            break;
        }  
    }

    return $response;
    $db= null;

    }catch(PDOException $e){
    echo $e->getMessage();
    }
});
function existeConfiguracionMeritos($idConvo,$idSeccion,$db){
    $sqlAux = "SELECT * FROM meritos_seccion WHERE id_conv='$idConvo' AND id_seccion='$idSeccion'";
    $resultado = $db->query($sqlAux);
    $tam = $resultado->rowCount();
    if($tam > 0){
        return true;
    }else{
        return false;
    }
}
function verificarPuntos($idConvo,$puntoParaAñadir,$db){
    $sql2 ="SELECT SUM(puntos) as puntosAcumulados FROM meritos_seccion WHERE id_conv='$idConvo'";
    $resultado = $db->query($sql2);
    $tam = $resultado->rowCount();
    if($tam > 0){
        $seccion = $resultado->fetchAll(PDO::FETCH_COLUMN);
        $puntosAcumulados = $seccion[0];
        if($puntosAcumulados < 100){
            $sumaDePuntos= $puntoParaAñadir+$puntosAcumulados;
            if($sumaDePuntos <= 100 ){
                //print("Puntos para añadir:".$puntoParaAñadir);
                return 1;            
            }else{
                //print("No se puede añadir, la suma es:".$sumaDePuntos);
                return 3;
            }
        }else{
            return 3;
        }
    }else{
        return 2;
    }
}

function eliminarConfiguracionMeritos($idConvo,$idSeccion,$db){
    $sql = "DELETE FROM meritos_seccion
    WHERE id_conv='$idConvo' AND id_seccion = '$idSeccion'";
    try{
        $db = new db();
        $db = $db->connectDB();
            $delete = $db->prepare($sql);
            $delete->bindParam("id_conv",$idConvo);
            $delete->execute();
        $db= null;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
}


$app->get("/listarSecciones", function (Request $request, Response $response, $args=[]) {
    
    $sql = "SELECT * FROM seccion_merito";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $items = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($items));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get("/sumaSecciones/{idConv}", function (Request $request, Response $response, $args=[]) {
    $idConv = $request->getAttribute("idConv");
    $sql = "SELECT SUM(puntos) as puntosAcumulados FROM meritos_seccion WHERE id_conv='$idConv '";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $suma = $resultado->fetchAll(PDO::FETCH_COLUMN);
            $response->getBody()->write(json_encode($suma));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get("/listarPuntosSecciones/{idConv}", function (Request $request, Response $response, $args=[]) {
    $idConv = $request->getAttribute("idConv");
    $sql = "SELECT *  FROM meritos_seccion WHERE id_conv='$idConv'
            ORDER BY id_seccion";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $suma = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($suma));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
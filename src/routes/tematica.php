<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->post('/crearTematica', function (Request $request, Response $response, $args) {

    $newTheme = json_decode($request->getBody());
    $puntoParaAñadir=$newTheme->portion;
    $idItem=$newTheme->idItem;
    $sql = "INSERT INTO tematica (id, id_item, nombre, porcentaje)
            VALUES (NULL, :id_item, :nombre, :porcentaje)";
    try{
        $db = new db();
        $db = $db->connectDB();
        switch(verificarPuntosTematica($idItem,$puntoParaAñadir,$db)){
            case 1:
                $insert = $db->prepare($sql);
                $insert->bindParam("id_item",$newTheme->idItem);
                $insert->bindParam("nombre",$newTheme->name);
                $insert->bindParam("porcentaje",$newTheme->portion);
                $insert->execute();
                $response->getBody()->write(json_encode("Succes"));
            break;
            case 2:
                $insert = $db->prepare($sql);
                $insert->bindParam("id_item",$newTheme->idItem);
                $insert->bindParam("nombre",$newTheme->name);
                $insert->bindParam("porcentaje",$newTheme->portion);
                $insert->execute();
                $response->getBody()->write(json_encode("Succes"));
            break;
            case 3:
                $response->getBody()->write(json_encode("Fail")); 
            break;
        }
        return $response;
        $db= null;

    }catch(PDOException $e){
        echo $e->getMessage();
    }
});


function verificarPuntosTematica($idItem,$puntoParaAñadir,$db){
    $sql2 ="SELECT SUM(porcentaje) as puntosAcumulados FROM tematica WHERE id_item='$idItem'";
    $resultado = $db->query($sql2);
    $tam = $resultado->rowCount();
    if($tam > 0){
        $seccion = $resultado->fetchAll(PDO::FETCH_COLUMN);
        $puntosAcumulados = $seccion[0];
        if($puntosAcumulados < 100){
            $sumaDePuntos= $puntoParaAñadir+$puntosAcumulados;
            if($sumaDePuntos <= 100 ){
                //print("Puntos para añadir:".$puntoParaAñadir);
                return 1;            
            }else{
                //print("No se puede añadir, la suma es:".$sumaDePuntos);
                return 3;
            }
        }else{
            return 3;
        }
    }else{
        return 2;
    }
}

$app->get("/sumaTematica/{idItem}", function (Request $request, Response $response, $args=[]) {
    $idItem = $request->getAttribute("idItem");
    $sql = "SELECT SUM(porcentaje) as puntosAcumulados FROM tematica WHERE id_item='$idItem'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $suma = $resultado->fetchAll(PDO::FETCH_COLUMN);
            $response->getBody()->write(json_encode($suma));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get("/listarTematicas/{idItem}", function (Request $request, Response $response, $args=[]) {
    $idItem = $request->getAttribute("idItem");
    $sql = "SELECT * FROM tematica WHERE id_item='$idItem'";
    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $convocatorias = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($convocatorias));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
$app->post('/eliminarTematica', function (Request $request, Response $response, $args) {

    $tematicaForm = json_decode($request->getBody());
    $idTematica = $tematicaForm->id;
    $sql = "DELETE FROM tematica
                    WHERE id='$idTematica'";
    try{
    $db = new db();
    $db = $db->connectDB();
        $delete = $db->prepare($sql);
        $delete->bindParam("id",$tematicaForm->id);
        $delete->execute();
        $response->getBody()->write(json_encode("Succes Delete"));
    return $response;
    $db= null;
    }catch(PDOException $e){
    echo $e->getMessage();
    }
});



$app->post('/aniadirNotaTematica', function (Request $request, Response $response, $args) {

    $newRendimiento = json_decode($request->getBody());
    $idTemtaica=$newRendimiento->idTematica;
    $notaParaAñadir=$newRendimiento->nota;
    $idUser=$newRendimiento->idUser;
    $idItem=$newRendimiento->idItem;
    $sql = "INSERT INTO nota_tematica (id, id_user,id_item, id_tematica, nota)
            VALUES (NULL,:id_user,:id_item,:id_tematica,:nota)";
    try{
    $db = new db();
    $db = $db->connectDB();
    if(existeUsuarioConTematica($idTemtaica,$idUser,$idItem,$db) == 0){
        switch(verificarNotaTematica($idTemtaica,$notaParaAñadir,$db)){
            case 1:
                $insert = $db->prepare($sql);
                $insert->bindParam("id_user",$newRendimiento->idUser);
                $insert->bindParam("id_item",$newRendimiento->idItem);
                $insert->bindParam("id_tematica",$newRendimiento->idTematica);
                $insert->bindParam("nota",$newRendimiento->nota);
                $insert->execute();
                $response->getBody()->write(json_encode("Succes"));
            break;
            case 2:
                $response->getBody()->write(json_encode("Fail")); 
            break;
            case 3:
                $response->getBody()->write(json_encode("EmptyNota")); 
            break;
        }
    }else{
        $response->getBody()->write(json_encode("1"));//usuario ya registrado
    }
    return $response;
    $db= null;

    }catch(PDOException $e){
    echo $e->getMessage();
    }
});
function existeUsuarioConTematica($idTematica,$idUser,$idItem,$conexion){
    $sql = "SELECT * 
            FROM nota_tematica
            WHERE id_tematica = '$idTematica' AND id_user='$idUser' AND id_item='$idItem'";
    $result = $conexion->query($sql);
    $tamaño = $result->rowCount();
    if($tamaño > 0){
        return 1;
    }else{
        return 0;
    }
}
function verificarNotaTematica($idTematica,$notaParaAñadir,$db){
    $sql2 ="SELECT porcentaje FROM tematica WHERE id='$idTematica'";
    $resultado = $db->query($sql2);
    $tam = $resultado->rowCount();
    if($tam > 0){
        $porcentajeMax = $resultado->fetchAll(PDO::FETCH_COLUMN);
        $notaMaxima = $porcentajeMax[0];
        if($notaParaAñadir <= $notaMaxima){
            //print("Puntos para añadir:".$puntoParaAñadir);
            return 1;       
        }else{
            return 2;
        }
    }else{
        return 3;
    }
}
$app->get("/listarNotaTematica/{idUser}/{idItem}", function (Request $request, Response $response, $args=[]) {
    $user = $request->getAttribute("idUser");
    $item = $request->getAttribute("idItem");
    $sql = "SELECT T.nombre,T.porcentaje,N.nota
            FROM nota_tematica AS N, tematica AS T
            WHERE N.id_user='$user' AND N.id_item='$item' AND N.id_tematica=T.id";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $notaUser = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($notaUser));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get("/sumarNotasTematica/{idUser}/{idItem}", function (Request $request, Response $response, $args=[]) {
    $idUser = $request->getAttribute("idUser");
    $item = $request->getAttribute("idItem");
    $sql = "SELECT SUM(nota) as notaTotal FROM nota_tematica WHERE id_user='$idUser' AND id_item='$item'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $suma = $resultado->fetchAll(PDO::FETCH_COLUMN);
            $response->getBody()->write(json_encode($suma));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
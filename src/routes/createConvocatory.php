<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->post('/createConvocatory', function (Request $request, Response $response, $args) {

    $newConvocatory = json_decode($request->getBody());
    
    $sql = "INSERT INTO convocatoria (id, nombre_conv, facultad, carrera, departamento, fecha_actual, fecha_limite)
    VALUES (NULL,:nombre_conv,:facultad,:carrera,:departamento,:fecha_actual, :fecha_limite)";
    try{
    $db = new db();
    $db = $db->connectDB();
        $insert = $db->prepare($sql);
        $insert->bindParam("nombre_conv",$newConvocatory->nameConv);
        $insert->bindParam("facultad",$newConvocatory->facultad);
        $insert->bindParam("carrera",$newConvocatory->carrera);
        $insert->bindParam("departamento",$newConvocatory->departamento);
        $insert->bindParam("fecha_actual",$newConvocatory->dateActual);
        $insert->bindParam("fecha_limite",$newConvocatory->dateLimit);
        $insert->execute();
        $response->getBody()->write(json_encode("Succes"));
    return $response;
    $db= null;

    }catch(PDOException $e){
    echo $e->getMessage();
    }

    });
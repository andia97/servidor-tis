<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

//Recibimos el Codigo de convocatoria y respondemos con un array de requisitos
$app->post('/getObligatorios', function (Request $request, Response $response) {
  $idConvocatoria = $request->getParam('idConvocatoria');
  $sql = "SELECT * FROM requisitoobligatorio WHERE requisitoobligatorio.id IN (SELECT id_req FROM agregarrequisito WHERE id_conv='$idConvocatoria')";

  try {
    $db = new db();
    $db = $db->connectDB();
    $resultado = $db->query($sql);
    $row = $resultado->fetchAll(PDO::FETCH_OBJ);
    $response->getBody()->write(json_encode($row));
    return $response;

    $row = null;
    $resultado = null;
    $db = null;
  } catch(PDOException $e) {
    echo '{"error" : {"text":'.$e->getMessage().'}';
  }
});

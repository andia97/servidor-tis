<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/listReqRequired', function (Request $request, Response $response, $args) {

    $sql = "SELECT * FROM requisitoobligatorio";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $reqObligatorios = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($reqObligatorios));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->post('/addReqRequired', function (Request $request, Response $response, $args) {

    $newAddReq = json_decode($request->getBody());
    $idConv = $newAddReq->idConv;
    $idReq = $newAddReq->idReq;
    $sql = "INSERT INTO agregarrequisito (id, id_req, id_conv,estado)
            VALUES (NULL,:id_req,:id_conv,:estado)";
    try{
        $db = new db();
        $db = $db->connectDB();
        if(existeReq($idConv,$idReq,$db) == 0){
            $insert = $db->prepare($sql);
            $insert->bindParam("id_req",$newAddReq->idReq);
            $insert->bindParam("id_conv",$newAddReq->idConv);
            $insert->bindParam("estado",$newAddReq->estado);
            $insert->execute();
            $response->getBody()->write(json_encode("Succes"));
        }else{
            $response->getBody()->write(json_encode("1"));//Ya esta participando en la convocatoria
        }
           
        return $response;
        $db= null;

    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
function existeReq($idConv,$idReq,$conexion){
    $sql = "SELECT *
            FROM agregarrequisito 
            WHERE id_req='$idReq' And id_conv='$idConv'";
    $result = $conexion->query($sql);
    $tamaño = $result->rowCount();
    if($tamaño > 0){
        return 1;
    }else{
        return 0;
    }
}
$app->get('/listAddRequired/{idConv}', function (Request $request, Response $response, $args) {
    $idConv = $request->getAttribute("idConv");
    $sql = "SELECT R.nombre,A.id, A.id_req,A.estado
            FROM agregarrequisito as A,requisitoobligatorio as R 
            WHERE A.id_req=R.id AND A.id_conv='$idConv'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $reqObligatorios = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($reqObligatorios));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->post('/deleteReq', function (Request $request, Response $response, $args) {
    $requestForm = json_decode($request->getBody());
    $id = $requestForm->id;
    //$id = $request->getAttribute("id");
    $sql = "DELETE FROM agregarrequisito
            WHERE id='$id'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $delete = $db->prepare($sql);
        $delete->bindParam("id",$id);
        $delete->execute();
        $response->getBody()->write(json_encode("Delete Succes"));
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
$app->post('/addReqRequiredExtra', function (Request $request, Response $response, $args) {

    $newAddReq = json_decode($request->getBody());
    $sql = "INSERT INTO agregarrequisito (id,id_conv,req_extra,estado)
            VALUES (NULL,:id_conv,:req_extra,:estado)";
    try{
        $db = new db();
        $db = $db->connectDB();
    
            $insert = $db->prepare($sql);
            $insert->bindParam("id_conv",$newAddReq->id_conv);
            $insert->bindParam("req_extra",$newAddReq->name_extra);
            $insert->bindParam("estado",$newAddReq->estado);
            $insert->execute();
            $response->getBody()->write(json_encode("Succes"));
           
        return $response;
        $db= null;

    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get('/listAddRequiredExtra/{idConv}', function (Request $request, Response $response, $args) {
    $idConv = $request->getAttribute("idConv");
    $sql = "SELECT A.id,A.req_extra,A.estado
            FROM agregarrequisito as A
            WHERE A.req_extra !='' and id_conv='$idConv' ";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $reqObligatorios = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($reqObligatorios));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});


$app->get('/listarRequisitosObligatoriosPresentados/{idInscrip}', function (Request $request, Response $response, $args) {
    $idInscripcion = $request->getAttribute("idInscrip");
    $sql = "SELECT I.id_user, I.id_conv, F.id AS idFile, F.id_insc_conv, F.name_req, F.file_dir,F.estado
            FROM inscripcionconvocatoria AS I, fileuploads AS F 
            WHERE I.id=F.id_insc_conv AND F.id_insc_conv='$idInscripcion' AND tipo='obligatorio'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $reqObligatorios = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($reqObligatorios));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->post('/rendimiento', function (Request $request, Response $response, $args) {

    $newRendimiento = json_decode($request->getBody());
    $idConvo=$newRendimiento->idConv;
    $puntoParaAñadir=$newRendimiento->point;
    $idSeccion=$newRendimiento->idSeccion;
    $sql = "INSERT INTO rendimiento_academico (id, id_conv, nombre, puntos)
            VALUES (NULL,:id_conv,:nombre,:puntos)";
    try{
    $db = new db();
    $db = $db->connectDB();
    switch(verificarPuntosRendimiento($idConvo,$puntoParaAñadir,$idSeccion,$db)){
        case 1:
            $insert = $db->prepare($sql);
            $insert->bindParam("id_conv",$newRendimiento->idConv);
            $insert->bindParam("nombre",$newRendimiento->name);
            $insert->bindParam("puntos",$newRendimiento->point);
            $insert->execute();
            $response->getBody()->write(json_encode("Succes"));
        break;
        case 2:
            $insert = $db->prepare($sql);
            $insert->bindParam("id_conv",$newRendimiento->idConv);
            $insert->bindParam("nombre",$newRendimiento->name);
            $insert->bindParam("puntos",$newRendimiento->point);
            $insert->execute();
            $response->getBody()->write(json_encode("Succes"));
        break;
        case 3:
            $response->getBody()->write(json_encode("Fail")); 
        break;
    }
    return $response;
    $db= null;

    }catch(PDOException $e){
    echo $e->getMessage();
    }
});

function verificarPuntosRendimiento($idConvo,$puntoParaAñadir,$idSeccion,$db){
    $sql2 ="SELECT SUM(puntos) as puntosAcumulados FROM rendimiento_academico WHERE id_conv='$idConvo'";
    $resultado = $db->query($sql2);
    $tam = $resultado->rowCount();
    $puntosAsignadosSeccion = obtenerPuntosSeccion($idConvo,$idSeccion,$db);
    if($tam > 0){
        $seccion = $resultado->fetchAll(PDO::FETCH_COLUMN);
        $puntosAcumulados = $seccion[0];
        if($puntosAcumulados < $puntosAsignadosSeccion){
            $sumaDePuntos= $puntoParaAñadir+$puntosAcumulados;
            if($sumaDePuntos <= $puntosAsignadosSeccion ){
                //print("Puntos para añadir:".$puntoParaAñadir);
                return 1;            
            }else{
                //print("No se puede añadir, la suma es:".$sumaDePuntos);
                return 3;
            }
        }else{
            return 3;
        }
    }else{
        return 2;
    }
}
$app->get("/sumaRendimiento/{idConv}", function (Request $request, Response $response, $args=[]) {
    $oneConv = $request->getAttribute("idConv");
    $sql = "SELECT SUM(puntos) as puntosAcumulados FROM rendimiento_academico WHERE id_conv='$oneConv'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $suma = $resultado->fetchAll(PDO::FETCH_COLUMN);
            $response->getBody()->write(json_encode($suma));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get("/listarRendimientoAcademico/{idConv}", function (Request $request, Response $response, $args=[]) {
    $idConvocatoria = $request->getAttribute("idConv");
    $sql = "SELECT * FROM rendimiento_academico WHERE id_conv='$idConvocatoria'";
    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $convocatorias = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($convocatorias));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->post('/eliminarRendimiento', function (Request $request, Response $response, $args) {

    $newRendimiento = json_decode($request->getBody());
    $idRendimiento = $newRendimiento->idReq;
    $sql = $sql = "DELETE FROM rendimiento_academico
                    WHERE id='$idRendimiento'";
    try{
    $db = new db();
    $db = $db->connectDB();
        $delete = $db->prepare($sql);
        $delete->bindParam("id",$newRendimiento->idReq);
        $delete->execute();
        $response->getBody()->write(json_encode("Succes Delete"));
    return $response;
    $db= null;
    }catch(PDOException $e){
    echo $e->getMessage();
    }
});

$app->post('/aniadirNotaRendimiento', function (Request $request, Response $response, $args) {

    $newRendimiento = json_decode($request->getBody());
    $idRendi=$newRendimiento->idRend;
    $notaParaAñadir=$newRendimiento->nota;
    $idUser=$newRendimiento->idUser;
    $sql = "INSERT INTO nota_rendimiento (id, id_user, id_rendimiento,id_conv, nota)
            VALUES (NULL,:id_user,:id_rendimiento,:id_conv,:nota)";
    try{
    $db = new db();
    $db = $db->connectDB();
    if(existeUsuarioConRendimiento($idRendi,$idUser,$db) == 0){
        switch(verificarNotaRendimiento($idRendi,$notaParaAñadir,$db)){
            case 1:
                $insert = $db->prepare($sql);
                $insert->bindParam("id_user",$newRendimiento->idUser);
                $insert->bindParam("id_rendimiento",$newRendimiento->idRend);
                $insert->bindParam("id_conv",$newRendimiento->idConv);
                $insert->bindParam("nota",$newRendimiento->nota);
                $insert->execute();
                $response->getBody()->write(json_encode("Succes"));
            break;
            case 2:
                $response->getBody()->write(json_encode("Fail")); 
            break;
            case 3:
                $response->getBody()->write(json_encode("EmptyNota")); 
            break;
        }
    }else{
        $response->getBody()->write(json_encode("1"));//usuario ya registrado
    }
    return $response;
    $db= null;

    }catch(PDOException $e){
    echo $e->getMessage();
    }
});
function existeUsuarioConRendimiento($idRendi,$idUser,$conexion){
    $sql = "SELECT * 
            FROM nota_rendimiento
            WHERE id_rendimiento = '$idRendi' AND id_user='$idUser'";
    $result = $conexion->query($sql);
    $tamaño = $result->rowCount();
    if($tamaño > 0){
        return 1;
    }else{
        return 0;
    }
}
function verificarNotaRendimiento($idRendi,$notaParaAñadir,$db){
    $sql2 ="SELECT puntos FROM rendimiento_academico WHERE id='$idRendi'";
    $resultado = $db->query($sql2);
    $tam = $resultado->rowCount();
    if($tam > 0){
        $puntosMax = $resultado->fetchAll(PDO::FETCH_COLUMN);
        $notaMaxima = $puntosMax[0];
        if($notaParaAñadir <= $notaMaxima){
            //print("Puntos para añadir:".$puntoParaAñadir);
            return 1;       
        }else{
            return 2;
        }
    }else{
        return 3;
    }
}
$app->get("/listarNotaRendimiento/{idUser}/{idConv}", function (Request $request, Response $response, $args=[]) {
    $user = $request->getAttribute("idUser");
    $conv = $request->getAttribute("idConv");
    $sql = "SELECT R.nombre,R.puntos,N.nota
            FROM nota_rendimiento AS N, rendimiento_academico AS R 
            WHERE N.id_user='$user' AND N.id_conv='$conv' AND N.id_rendimiento=R.id AND N.id_conv=R.id_conv";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $notaUser = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($notaUser));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});

$app->get("/sumarNotas/{idUser}/{idConv}", function (Request $request, Response $response, $args=[]) {
    $idUser = $request->getAttribute("idUser");
    $conv = $request->getAttribute("idConv");
    $sql = "SELECT SUM(nota) as notaTotal FROM nota_rendimiento WHERE id_user='$idUser' AND id_conv='$conv'";

    try{
        $db = new db();
        $db = $db->connectDB();
        $resultado = $db->query($sql);
        $tam = $resultado->rowCount();
        if($tam > 0){
            $suma = $resultado->fetchAll(PDO::FETCH_COLUMN);
            $response->getBody()->write(json_encode($suma));
        }else{
            $response->getBody()->write(json_encode("Empty"));
        }
        return $response;
    }catch(PDOException $e){
        echo $e->getMessage();
    }
});
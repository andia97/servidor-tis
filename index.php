<?php

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

  require 'vendor/autoload.php';
  require 'src/config/db.php';

	$config = ["settings" => [
		"displayErrorDetails" => true
	]];

  $app = new Slim\App($config);

  $app->add(function ($req, $res, $next) {
	$response = $next($req, $res);
	return $response
		->withHeader('Access-Control-Allow-Origin', '*')
		->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
		->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  });

  $app->get('/', function (Request $request, Response $response, $args) {
    return $response->getBody()->write("API Works.");
  });

  //Endpoints de codigo
  require 'src/routes/codigo.php';

  //Endpoints auntenticar usuario
  require 'src/routes/autentificarUsuarios.php';
  //Endpoints listar usuarios
  require 'src/routes/listarUsuarios.php';
  //Endpoints registrar usuarios
  require 'src/routes/registrarUsuarios.php';
  //Endpoints crear convocatorias
  require 'src/routes/createConvocatory.php';
    //Endpoints crear listar convocatorias
  require 'src/routes/listConvocatorys.php';
    //Endpoints crear crear item
  require 'src/routes/createItem.php';
    //Endpoints crear inscrpcion a una convocatoria
  require 'src/routes/inscriptionConvocatory.php';
    //Endpoints crear inscrpcion a una convocatoria
  require 'src/routes/listConvocaPostulaciones.php';
    //Endpoints crear lista de items por convocatoria
  require 'src/routes/listItemsConvocatory.php';
    //Endpoints crear lista de requisitos obligatorios
  require 'src/routes/listRequirementsRequired.php';
    //Endpoints crear registrar requisitos opcionales
  require 'src/routes/requirementOptional.php';
    //Requisitos Obligatorios filtrados por convocatoria
  require 'src/routes/requisitosObligatorios.php';
    //Tematicas por itmes
  require 'src/routes/tematica.php';
    //Roles de comision
  require 'src/routes/rolComision.php';
    //Roles de comision
  require 'src/routes/redimientoAcademico.php';
    //Habilitar o inhabilitar a un postulante
  require 'src/routes/habilitados.php';
    //Habilitar o inhabilitar a un postulante
  require 'src/routes/inscripcion_item.php';
    //Habilitar o inhabilitar a un postulante
  require 'src/routes/seccion_meritos.php';
  require 'src/routes/fileUpload.php';
  $app->run();
